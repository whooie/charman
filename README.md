# charman

Curses-based terminal interface for managing Adventure Academy-style DnD character
sheets. There's also some cool extra functionality like a cool commandline interface
featuring quick-and-easy rolling syntax and cool note-taking.

Currently, only rolling through the CLI interface has been implemented.

## Installation

### Dependencies
- python >= 3.7.3
- curses
- pyyaml

As dirty as it makes me feel, the `charman.py` program was written as a single
script for maximum portability. Unfortunately, `curses` and `yaml` are not standard
modules for a python on Windows, so install them with (python3's) `pip`:

```
python -m pip install windows-curses
python -m pip install pyyaml
```

If you don't have `pip` installed, install it with

```
python -m ensurepip --default-pip
```

Then `charman.py` can be run without issue, assuming an otherwise standard python
installation.

*Note: all the Windows stuff is only minimally tested because I can't be fucked
to install Windows, so I'm also going to link a StackOverflow page with another
potential `curses` solution
[here](https://stackoverflow.com/questions/32417379/what-is-needed-for-curses-in-python-3-4-on-windows7).*

## Usage

`charman` can be launched as a standard python script from the shell with either
the full terminal or commandline interface. See the output of

```
python charman.py --help
```

for details.

*Note: due to the developmental state of the program (we'll call it alpha, but
I'm not sure if it even qualifies for that), the default behavior is to launch in
CLI mode.*


## Character Data Storage

Character sheets are read and written by the program in `yaml` format. If you're
unfamiliar with the format, I suggest you take a look at the
[wikipedia](https://en.wikipedia.org/wiki/YAML#Syntax) page. An example character
sheet file can be generated with the `--generate-char` option. Unfortunately, this
file isn't the most human-readable, so another example file, `example.yml`, is
included as well.

## To Do

- Add a fuck-ton of commands to finish out the CLI interface
    - ~~`load`/`save` for reading/writing character files from within the program~~
    - ~~`get`/`set` to manage character properties~~
    - ~~`add`/`remove`/`use` to manage inventory items~~
    - ~~`equip`/`unequip` for armors and shields~~
    - ~~`Help` and `help <command>` text~~
    - Add a framework to keep track of courses for each term (maybe)
    - ~~Add `less`-like behavior for help texts~~
- Learn more `curses` some more and write the whole interface
    - integrate the CLI commands with context-dependent keyboard shortcuts
    - write printing/scrolling functions for suitably efficient keyboard navigation
      through the different elements of characters

## Known Bugs

- Pressing backspace to remove a character from the end of a line that has been
  scrolled to maximum width causes an empty space to appear even though the characters
  it covers have still been recorded
- ~~Pressing backspace from Windows command prompt doesn't work~~
- Cursor position and display offset don't readjust when the window size changes.
  Not sure if it's worth the effort to fix right now.
- Setting durability values to "inf" and then back to integer values has some weird
  but (so far) non-fatal results
- Screen doesn't update properly after quitting a help display on Windows
