#!/usr/bin/python

import sys
import getopt
import math as m
import random
from collections import defaultdict as ddict
import copy
try:
    import yaml
except ImportError:
    print("Failed to load the system's pyyaml module.")
    print("Check that it is installed correctly.")
    sys.exit(1)
try:
    import curses as cu
    import curses.panel as pa
    import curses.ascii as asc
except ImportError:
    print("Failed to load the system's curses module.")
    print("Check that it is installed correctly.")
    sys.exit(1)

def rank(eff):
    """
    Calculate rank given effort
    """
    return 0 if eff == 0 else int(eff/m.fabs(eff) * (m.sqrt(m.fabs(eff) + 1.0) - 1))

def morale(comp):
    """
    Calculate morale given composure
    """
    return int(rank(comp)/2)

def get_weight(w):
    """
    Return weight as a string from string or int/float input.
    """
    if type(w) == type(int()) or type(w) == type(float()):
        if int(w) == 1:
            return "Light"
        elif int(w) == 2:
            return "Medium"
        elif int(w) == 3:
            return "Heavy"
        elif w < 1:
            return "Feather"
        else:
            return "Anvil"
    else:
        return str(w)

def get_size(s):
    """
    Return size as a string from string or int/float input.
    """
    if type(s) == type(int()) or type(s) == type(float()):
        if int(s) == 1:
            return "Small"
        elif int(s) == 2:
            return "Medium"
        elif int(s) == 3:
            return "Large"
        elif s < 1:
            return "Tiny"
        else:
            return "Huge"
    else:
        return str(s)

def get_durability(d):
    """
    Return durability as a two-item list from string or list input.
    """
    if type(d) == type(str()):
        if d.lower() == "inf":
            return [m.inf, m.inf]
        else:
            return [0,0]
    elif d == None:
        return [0,0]
    else:
        return d

def get_yaml_durability(d):
    """
    Return durability in a yaml config-compatible format.
    """
    if d == [m.inf, m.inf]:
        return "Inf"
    else:
        return d

def get_ammo(a):
    """
    Return ammo as an int from string or int input.
    """
    if type(a) == type(str()):
        if a.lower() == "inf":
            return m.inf
        else:
            return 0
    elif a == None:
        return 0
    else:
        return a

def get_yaml_ammo(a):
    """
    Return ammo in a yaml config-compatible format.
    """
    if a == m.inf:
        return "Inf"
    else:
        return a

def get_description(d):
    """
    Return a description string with trailing newlines removed.
    """
    if d == None:
        return "None"
    if d == "":
        return d
    description = d
    if description[-1] == "\n":
        description = description [:-1]
    return description

def is_valid_dice(expr):
    """
    Return True if expr can be interpreted as a dice expression, False otherwise
    """
    try:
        dsplit = expr.split('d')
        n = int(dsplit[0])
        if '+' in dsplit[1]:
            modsplit = dsplit[1].split('+')
            k = int(modsplit[0])
            m = int(modsplit[1])
        elif '-' in dsplit[1]:
            modsplit = dsplit[1].split('-')
            k = int(modsplit[0])
            m = int(modsplit[1])
        else:
            k = int(dsplit[1])
        return True
    except:
        return False

def parse_dice(expr):
    """
    Returns the n,k,m components of a dice expression
    """
    if is_valid_dice(expr):
        m = 0
        dsplit = expr.split('d')
        n = int(dsplit[0])
        if '+' in dsplit[1]:
            modsplit = dsplit[1].split('+')
            k = int(modsplit[0])
            m = int(modsplit[1])
        elif '-' in dsplit[1]:
            modsplit = dsplit[1].split('-')
            k = int(modsplit[0])
            m = -int(modsplit[1])
        else:
            k = int(dsplit[1])
        return (n,k,m)
    else:
        raise Exception('invalid dice expression')

def dice_max(expr):
    """
    Return the maximum value of a dice expression
    """
    if is_valid_dice(expr):
        (n,k,m) = parse_dice(expr)
        return n*k+m
    else:
        raise Exception('invalid dice expression')

def roll_expr(expr):
    """
    Roll a single dice expression
    
    INPUTS:
        expr - dice expression string for n dice of each k sides
               can optionally be of the form ndk+/-m
               n,k,m must be integers

    OUTPUTS: (total, total_m, rolls)
        total   - natural total of the roll
        total_m - natural total plus modifier
                  this is included even if the modifier is zero
        rolls   - list of the natural rolls
    """
    if is_valid_dice(expr):
        (n,k,m) = parse_dice(expr)
    else:
        raise Exception('invalid dice expression')
    total = 0
    rolls = list()
    for i in range(n):
        roll = int(random.random()*k + 1)
        total = total + roll
        rolls.append(roll)
    total_m = total + m
    return (total, total_m, rolls)

def multiroll(exprs):
    """
    Performs a series of rolls
    
    INPUTS:
        exprs - list of dice expressions; see roll_expr()

    OUTPUTS: (rolls_max, rolls_min, totals, totals_m, all_rolls)
        totals    - list of all natural totals
        totals_m  - list of all totals (with modifiers)
        all_rolls - list of all natural roll lists
    """
    totals = list()
    totals_m = list()
    all_rolls = list()
    for expr in exprs:
        (total, total_m, rolls) = roll_expr(expr)
        totals.append(total)
        totals_m.append(total_m)
        all_rolls.append(rolls)
    return (totals, totals_m, all_rolls)
        
def roll_ad(expr, n=2, adv='+'):
    """
    Performs a multiple roll with advantage or disadvantage.
    Acts as a thin wrapper for multiroll()

    INPUTS:
        expr - dice expression; see roll_expr()
        n    - (optional)
               number of times to repeat the roll
        adv  - flag controlling the extremum selection
               adv = `+` rolls with advantage, adv = `-` rolls with disadvantage
               invalid values roll with advantage

    OUTPUTS: (rolls_max, rolls_max_m, totals, totals_m, all_rolls)
        rolls_ex    - max/min of all natural totals
        rolls_ex _m - max/min of all totals with modifiers
        totals      - list of all natural totals
        totals_m    - list of all totals (with modifiers)
        all_rolls   - list of all natural roll lists
    """
    exprs = list()
    for i in range(n):
        exprs.append(expr)
    (totals, totals_m, all_rolls) = multiroll(exprs)
    if adv == '+':
        rolls_ex = max(totals)
        rolls_ex_m = max(totals_m)
    elif adv == '-':
        rolls_ex = min(totals)
        rolls_ex_m = min(totals_m)
    return (rolls_ex, rolls_ex_m, totals, totals_m, all_rolls)

# used for loading from yaml
def safe_dict(adict, defval=None):
    safe = ddict(lambda: defval)
    if adict != None:
        safe = ddict(lambda: defval)
        for key in adict:
            safe[key] = adict[key]
    return safe

class CharInfo:
    """
    CharInfo class.

    Properties:
        str name
        str race
        str background
        str size
        int speed
        int term
        int credits
        bool inspiration
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_race = "None"
        def_background = "None"
        def_size = "None"
        def_speed = 1
        def_term = 1
        def_credits = 0
        def_inspiration = False
        
        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.race = def_race if safe["race"] == None else safe["race"]
        self.background = def_background if safe["background"] == None else safe["background"]
        self.size = get_size(safe["size"])
        self.speed = def_speed if safe["speed"] == None else safe["speed"]
        self.term = def_term if safe["term"] == None else safe["term"]
        self.credits = def_credits if safe["credits"] == None else safe["credits"]
        self.inspiration = def_inspiration if safe["inspiration"] == None else safe["inspiration"]

    def getall(self):
        return [
                self.name,
                self.race,
                self.background,
                self.term,
                self.size,
                self.speed,
                self.credits,
                self.inspiriation
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def get_yaml(self):
        return copy.deepcopy(
               {"name": self.name,
                "race": self.race,
                "background": self.background,
                "size": self.size,
                "speed": self.speed,
                "term": self.term,
                "credits": self.credits,
                "inspiration": self.inspiration
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Race: {self.race}\n" + \
               f"Background: {self.background}\n" + \
               f"Size: {self.size}\n" + \
               f"Speed: {self.speed}\n" + \
               f"Term: {self.term}\n" + \
               f"Credits: {self.credits}\n" + \
               f"Inspiration: {self.inspiration}"

class Stats:
    """
    Stats class.

    Properties:
        [int, int] hp
        [int, int] sp
        [int, int] cp
        [int, int] mp
        str status
    """
    def __init__(self, yaml_dict=None):
        def_status = "Healthy"

        safe = safe_dict(yaml_dict, [1,1])
        self.hp = safe["hp"]
        self.sp = safe["sp"]
        self.cp = safe["cp"]
        self.mp = safe["mp"]
        self.status = def_status if safe["status"] == [1,1] else safe["status"]

    def getall(self):
        return [
                self.hp,
                self.sp,
                self.cp,
                self.mp,
                self.status
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def get_yaml(self):
        return copy.deepcopy(
               {"hp": self.hp,
                "cp": self.cp,
                "sp": self.sp,
                "mp": self.mp,
                "status": self.status
                })

    def __str__(self):
        return f"HP: {self.hp[0]} / {self.hp[1]}\n" + \
               f"CP: {self.cp[0]} / {self.cp[1]}\n" + \
               f"SP: {self.sp[0]} / {self.sp[1]}\n" + \
               f"MP: {self.mp[0]} / {self.mp[1]}\n" + \
               f"Status: {self.status}"

class Chart:
    """
    Chart class.
    
    Properties:
        Stats obj stats
        [[str]] aa - chart position -> dict key
        defaultdict{str: int} chart_aa - chart values by aa string
        [[int]] chart_nn - raw chart values by position
        [[str]] names - display names with proper capitalization
        int height
        int width
    """
    def __init__(self, attr=None, asp=None, skills=None, aa=None, names=None):
        # build an initial dictionary with everything defined in the yaml file
        self.chart_aa = ddict(int)
        if attr != None:
            for key in attr:
                self.chart_aa[key] = attr[key]
        if asp != None:
            for key in asp:
                self.chart_aa[key] = asp[key]
        if skills != None:
            for key in skills:
                self.chart_aa[key] = skills[key]

        # we need to keep track of which string labels apply to which cell
        self.aa = [
                ['physical',  'str',         'dex',       'con'],
                ['offense',   'strike',      'aim',       'grapple'],
                ['mobility',  'force',       'sneak',     'trek'],
                ['defense',   'parry',       'evade',     'block'],
                ['social',    'cha',         'emp',       'tem'],
                ['persuade',  'charm',       'negotiate', 'aggro'],
                ['diplomacy', 'barter',      'insight',   'interrogate'],
                ['deceive',   'mislead',     'disguise',  'suspect'],
                ['mental',    'int',         'wis',       'foc'],
                ['craft',     'appraise',    'medicine',  'engineer'],
                ['awareness', 'investigate', 'survive',   'perceive'],
                ['acuity',    'knowledge',   'resolve',   'concentrate'],
                ['spr',       'spellcast',   'ritual',    'artifact']
                ] if aa == None else aa
        # unfortunately we need this because capitalization schemes differ
        # between cells
        self.names = [
                ['PHYSICAL',  'STR',         'DEX',       'CON'],
                ['Offense',   'Strike',      'Aim',       'Grapple'],
                ['Mobility',  'Force',       'Sneak',     'Trek'],
                ['Defense',   'Parry',       'Evade',     'Block'],
                ['SOCIAL',    'CHA',         'EMP',       'TEM'],
                ['Persuade',  'Charm',       'Negotiate', 'Aggro'],
                ['Diplomacy', 'Barter',      'Insight',   'Interrogate'],
                ['Deceive',   'Mislead',     'Disguise',  'Suspect'],
                ['MENTAL',    'INT',         'WIS',       'FOC'],
                ['Craft',     'Appraise',    'Medicine',  'Engineer'],
                ['Awareness', 'Investigate', 'Survive',   'Perceive'],
                ['Acuity',    'Knowledge',   'Resolve',   'Concentrate'],
                ['SPIRIT',    'Spellcast',   'Ritual',    'Artifact']
                ] if names == None else names
        self.height = len(self.aa)
        self.width = len(self.aa[0])
        
        # build an int version of aa
        self.chart_nn = list()
        for i in range(self.height):
            row = list()
            for j in range(self.width):
                row.append(self.chart_aa[self.aa[i][j]])
            self.chart_nn.append(row)
    
    # find the (i,j) position of a string if it's a valid cellname
    def getpos(self, cellname):
        i = -1; j = -1
        for m in range(self.height):
            if cellname in self.aa[m]:
                i = m
                j = self.aa[m].index(cellname)
                break
        if i == -1 or j == -1:
            raise Exception('invalid chart position')
        return (i,j)

    # get the properly capitalized name of a cell from either an (i,j) position
    # or a cellname string
    def getname(self, pos):
        try:
            if type(pos) == type(tuple()):
                i,j = pos
            elif type(pos) == type(str()):
                (i,j) = self.getpos(pos.lower())
            else:
                raise Exception('invalid chart position')
            return self.names[i][j]
        except:
            raise Exception('invalid chart position')
    
    # get the mod of a cell from either an (i,j) position or a cellname string
    def getmod(self, pos):
        try:
            if type(pos) == type(tuple()):
                i,j = pos
            elif type(pos) == type(str()):
                (i,j) = self.getpos(pos.lower())
            else:
                raise Exception('invalid chart position')
            if (i < 12 and i % 4 == 0) or j == 0:
                return rank(self.chart_nn[i][j])
            elif i == 12:
                return rank(self.chart_nn[i][j]) \
                        + rank(self.chart_nn[i][0])
            else:
                return self.chart_nn[i][j] \
                        + rank(self.chart_nn[i - i % 4][j]) \
                        + rank(self.chart_nn[i][0])
        except:
            raise Exception('invalid chart position')

    # native python [pos] syntax to get the raw value in the nn chart where pos
    # is either the (i,j) position or cellname string
    def __getitem__(self, pos):
        try:
            if type(pos) == type(str()):
                (i,j) = self.getpos(pos.lower())
            else:
                i,j = pos
                if i < 0 or i >= self.height or j < 0 or j >= self.width:
                    raise Exception('invalid chart position')
            return self.chart_nn[i][j]
        except:
            raise Exception('invalid chart position')

    # native python [pos] syntax to set the raw value in the nn and aa charts
    def __setitem__(self, pos, val):
        try:
            if type(pos) == type(str()):
                aa = pos.lower()
                (i,j) = self.getpos(aa)
            else:
                i,j = pos
                aa = self.aa[i][j]
        except:
            raise Exception('invalid chart position')
        self.chart_nn[i][j] = val
        self.chart_aa[aa] = val

    def getall(self):
        return self.chart_nn

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def get_yaml(self):
        attributes = dict()
        aspects = dict()
        skills = dict()
        for i in range(self.height):
            for j in range(self.width):
                if i == 12:
                    if j == 0:
                        attributes[self.aa[i][j]] = self.chart_nn[i][j]
                    else:
                        aspects[self.aa[i][j]] = self.chart_nn[i][j]
                elif i % 4 == 0:
                    if j == 0:
                        continue
                    else:
                        attributes[self.aa[i][j]] = self.chart_nn[i][j]
                elif j == 0:
                    aspects[self.aa[i][j]] = self.chart_nn[i][j]
                else:
                    skills[self.aa[i][j]] = self.chart_nn[i][j]
        return (copy.deepcopy(attributes), copy.deepcopy(aspects), copy.deepcopy(skills))

    def __str__(self):
        outstr = 50*"=" + "\n"
        for i in range(self.height):
            topline = ""
            bottomline = ""
            for j in range(self.width):
                topline = topline + f"{self.names[i][j]:<12}"
                if i % 4 == 0 and j == 0 and i < 12:
                    bottomline = bottomline + 12*" "
                else:
                    bottomline = bottomline + f"{self.chart_nn[i][j]:<+12}"
                if j == 0:
                    topline = topline + "| "
                    bottomline = bottomline + "| "
            outstr = outstr + topline + "\n" + bottomline + "\n"
            if i % 4 == 0 and i < 12:
                outstr = outstr + 12*"-" + "+-" + 36*"-" + "\n"
            elif i % 4 == 3:
                outstr = outstr + 50*"=" + "\n"
        outstr = outstr + 50*"="
        return outstr

    # same as __str__, but with mod values instead of raw values
    def modstr(self):
        outstr = 50*"=" + "\n"
        for i in range(self.height):
            topline = ""
            bottomline = ""
            for j in range(self.width):
                topline = topline + f"{self.names[i][j]:<12}"
                if i % 4 == 0 and j == 0 and i < 12:
                    bottomline = bottomline + 12*" "
                else:
                    bottomline = bottomline + f"{self.getmod((i,j)):<+12}"
                if j == 0:
                    topline = topline + "| "
                    bottomline = bottomline + "| "
            outstr = outstr + topline + "\n" + bottomline + "\n"
            if i % 4 == 0 and i < 12:
                outstr = outstr + 12*"-" + "+-" + 36*"-" + "\n"
            elif i % 4 == 3:
                outstr = outstr + 50*"=" + "\n"
        outstr = outstr + 50*"="
        return outstr

class Armor:
    """
    Armor class

    Properties:
        str name
        int defense
        str weight
        [int,int] durability
        str effect
        str description
        bool equip
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_defense = 0
        def_weight = "None"
        def_durability = [0,0]
        def_effect = "None"
        def_description = "None"
        def_equip = False

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.defense = def_defense if safe["defense"] == None else safe["defense"]
        self.weight = get_weight(safe["weight"])
        self.durability = get_durability(safe["durability"])
        self.effect = def_effect if safe["effect"] == None else safe["effect"]
        self.description = get_description(safe["description"])
        self.equip = def_equip if safe["equip"] == None else safe["equip"]

    def getall(self):
        return [
                self.name,
                self.defense,
                self.weight,
                self.durability,
                self.effect,
                self.description,
                self.equip
                ]

    def get_yaml(self):
        return copy.deepcopy({
                "name": self.name,
                "defense": self.defense,
                "weight": self.weight,
                "durability": self.durability,
                "effect": self.effect,
                "description": self.description,
                "equip": self.equip
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Defense: {self.defense}\n" + \
               f"Weight: {self.weight}\n" + \
               f"Durability: {self.durability[0]} / {self.durability[1]}\n" + \
               f"Effect: {self.effect}\n" + \
               f"Description: {self.description}\n" + \
               f"Equip: {self.equip}"

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name < other.name
        elif self.defense != other.defense:
            return self.defense > other.defense
        elif self.equip or other.equip:
            return self.equip
        else:
            return False

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name > other.name
        elif self.defense != other.defense:
            return self.defense > other.defense
        elif self.equip or other.equip:
            return other.equip
        else:
            return False

    def __ge__(self, other):
        return (self > other) or (self == other)

class Shield:
    """
    Shield class

    Properties:
        str name
        int defense
        str weight
        [int,int] durability
        str effect
        str description
        bool equip
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_defense = 0
        def_weight = "None"
        def_durability = [0,0]
        def_effect = "None"
        def_description = "None"
        def_equip = False

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.defense = def_defense if safe["defense"] == None else safe["defense"]
        self.weight = get_weight(safe["weight"])
        self.durability = get_durability(safe["durability"])
        self.effect = def_effect if safe["effect"] == None else safe["effect"]
        self.description = get_description(safe["description"])
        self.equip = def_equip if safe["equip"] == None else safe["equip"]

    def getall(self):
        return [
                self.name,
                self.defense,
                self.weight,
                self.durability,
                self.effect,
                self.description,
                self.equip
                ]

    def get_yaml(self):
        return copy.deepcopy({
                "name": self.name,
                "defense": self.defense,
                "weight": self.weight,
                "durability": get_yaml_durability(self.durability),
                "effect": self.effect,
                "description": self.description,
                "equip": self.equip
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Defense: {self.defense}\n" + \
               f"Weight: {self.weight}\n" + \
               f"Durability: {self.durability[0]} / {self.durability[1]}\n" + \
               f"Effect: {self.effect}\n" + \
               f"Description: {self.description}\n" + \
               f"Equip: {self.equip}"

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name < other.name
        elif self.defense != other.defense:
            return self.defense > other.defense
        elif self.equip or other.equip:
            return self.equip
        else:
            return False

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name > other.name
        elif self.defense != other.defense:
            return self.defense > other.defense
        elif self.equip or other.equip:
            return other.equip
        else:
            return False

    def __ge__(self, other):
        return (self > other) or (self == other)

class Weapon:
    """
    Weapon class

    Properties:
        str name
        str damage - must be a valid dice expression
        str type
        int range
        int ammo
        str weight
        [int,int] durability
        str effect
        str description
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_damage = "1d0"
        def_type = "None"
        def_range = 0
        def_ammo = 0
        def_weight = "None"
        def_durability = [0,0]
        def_effect = "None"
        def_description = "None"

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.damage = def_damage if safe["damage"] == None else safe["damage"]
        self.type = def_type if safe["type"] == None else safe["type"]
        self.range = def_range if safe["range"] == None else safe["range"]
        self.ammo = get_ammo(safe["ammo"])
        self.weight = get_weight(safe["weight"])
        self.durability = get_durability(safe["durability"])
        self.effect = def_effect if safe["effect"] == None else safe["effect"]
        self.description = get_description(safe["description"])

    def getall(self):
        return [
                self.name,
                self.damage,
                self.type,
                self.range,
                self.ammo,
                self.weight,
                self.durability,
                self.effect
                ]

    def get_yaml(self):
        return copy.deepcopy({
                "name": self.name,
                "damage": self.damage,
                "type": self.type,
                "range": self.range,
                "ammo": get_yaml_ammo(self.ammo),
                "weight": self.weight,
                "durability": get_yaml_durability(self.durability),
                "effect": self.effect,
                "description": self.description
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Damage: {self.damage}\n" + \
               f"Type: {self.type}\n" + \
               f"Range: {self.range}\n" + \
               f"Ammo: {self.ammo}\n" + \
               f"Weight: {self.weight}\n" + \
               f"Durability: {self.durability[0]} / {self.durability[1]}\n" + \
               f"Effect: {self.effect}\n" + \
               f"Description: {self.description}"

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name < other.name
        elif self.type != other.type:
            return self.type < other.type
        elif dice_max(self.damage) != dice_max(other.damage):
            return dice_max(self.damage) > dice_max(other.defense)
        elif self.range != other.range:
            return self.range > other.range
        elif self.ammo != other.ammo:
            return self.ammo > other.ammo
        else:
            return False

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name > other.name
        elif self.type != other.type:
            return self.type > other.type
        elif dice_max(self.damage) != dice_max(other.damage):
            return dice_max(self.damage) < dice_max(other.defense)
        elif self.range != other.range:
            return self.range < other.range
        elif self.ammo != other.ammo:
            return self.ammo < other.ammo
        else:
            return False

    def __ge__(self, other):
        return (self > other) or (self == other)

class Item:
    """
    Item class

    Properties:
        str name
        str description
    """
    def __init__(self, yaml_dict=None):
        safe = safe_dict(yaml_dict, "None")
        self.name = safe["name"]
        self.description = get_description(safe["description"])

    def getall(self):
        return [
                self.name,
                self.description
                ]

    def get_yaml(self):
        return copy.deepcopy({
                "name": self.name,
                "description": self.description
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Description: {self.description}"

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name < other.name
        else:
            return False

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name > other.name
        else:
            return False

    def __ge__(self, other):
        return (self > other) or (self == other)

class Consumable:
    """
    Consumable class

    Properties:
        str name
        [int,int] uses
        int amount
        str description
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_uses = [1,1]
        def_amount = 1
        def_description = "None"

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.uses = def_uses if safe["uses"] == None else safe["uses"]
        self.amount = def_amount if safe["amount"] == None else safe["amount"]
        self.description = get_description(safe["description"])

    def getall(self):
        return [
                self.name,
                self.uses,
                self.amount,
                self.description
                ]

    def getinfo(self):
        return [
                self.name,
                self.uses,
                self.description
                ]

    def get_yaml(self):
        return copy.deepcopy({
                "name": self.name,
                "uses": self.uses,
                "amount": self.amount,
                "description": self.description
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Uses: {self.uses[0]} / {self.uses[1]}\n" + \
               f"Amount: {self.amount}\n" + \
               f"Description: {self.description}"

    def get_total_uses(self):
        return (self.amount-1)*self.uses[1] + self.uses[0]

    def __eq__(self, other):
        return self.getinfo() == other.getinfo() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name < other.name
        elif self.get_total_uses() != other.get_total_uses():
            return self.get_total_uses() > other.get_total_uses()
        else:
            return False

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name > other.name
        elif self.get_total_uses() != other.get_total_uses():
            return self.get_total_uses() < other.get_total_uses()
        else:
            return False

    def __ge__(self, other):
        return (self > other) or (self == other)

class Crystals:
    """
    Crystals class

    Properties:
        defaultdict{str: [int,int,int,int]} chart_aa
        [str] colors
        [str] sizes
        [[int]] chart_nn
    """
    def __init__(self, yaml_dict=None, colors=None, sizes=None):
        self.chart_aa = ddict(lambda: [0,0,0,0])
        if yaml_dict != None:
            for key in yaml_dict:
                self.chart_aa[key] = yaml_dict[key]

        self.colors = [
                "black",
                "red",
                "orange",
                "yellow",
                "green",
                "blue",
                "violet",
                "white",
                "clear"
                ] if colors == None else colors
        self.sizes = ["s", "m", "l", "x"] if sizes == None else sizes

        self.chart_nn = list()
        for color in self.colors:
            self.chart_nn.append(self.chart_aa[color])

    def getcolor(self, color):
        try:
            i = color
            if type(i) == type(str()):
                i = self.colors.index(i.lower())
            return self.chart_nn[i]
        except:
            raise Exception('invalid crystal specification')

    def getpos(self, color, size):
        try:
            i = self.colors.index(color.lower())
            j = self.sizes.index(size.lower())
            return (i,j)
        except:
            raise Exception('invalid crystal specification')

    def __getitem__(self, pos):
        try:
            i,j = pos
            if type(i) == type(str()):
                i = self.colors.index(i.lower())
            if type(j) == type(str()):
                j = self.sizes.index(j.lower())
            return self.chart_nn[i][j]
        except:
            raise Exception('invalid crystal specification')

    def __setitem__(self, pos, val):
        try:
            i,j = pos
            if type(i) == type(str()):
                ai = i
                i = self.colors.index(ai)
            else:
                ai = self.colors[i]
            if type(j) == type(str()):
                j = self.sizes.index(j)
        except:
            raise Exception('invalid crystal specification')
        self.chart_nn[i][j] = val
        self.chart_aa[ai][j] = val

    def get_yaml(self):
        res = dict()
        for color in self.colors:
            res[color] = self.chart_aa[color]
        return copy.deepcopy(res)

    def __str__(self):
        outstr = f"{'':8}"
        for j in range(len(self.sizes)):
            outstr = outstr + f"{self.sizes[j].capitalize():<4}"
        outstr = outstr + "\n"
        for i in range(len(self.colors)):
            outstr = outstr + f"{self.colors[i].capitalize():<8}"
            for j in range(len(self.sizes)):
                outstr = outstr + f"{self.chart_nn[i][j]:<4}"
            outstr = outstr + "\n"
        return outstr[:-1]

class Trait:
    """
    Trait class

    Properties:
        str name
        str type
        str description
    """
    def __init__(self, yaml_dict=None):
        safe = safe_dict(yaml_dict, "None")
        self.name = safe["name"]
        self.type = safe["type"]
        self.description = get_description(safe["description"])

    def getall(self):
        return [
                self.name,
                self.type,
                self.description
                ]

    def get_yaml(self):
        return copy.deepcopy({
                "name": self.name,
                "type": self.type,
                "description": self.description
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Type: {self.type}\n" + \
               f"Description: {self.description}"

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name < other.name
        elif self.type != other.type:
            return self.type < other.type
        else:
            return False

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name > other.name
        elif self.type != other.type:
            return self.type > other.type
        else:
            return False

    def __ge__(self, other):
        return (self > other) or (self == other)

class Ability:
    """
    Ability class

    Properties:
        str name
        str type
        int cost
        str description
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_type = "None"
        def_cost = 0
        def_description = "None"

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.type = def_type if safe["type"] == None else safe["type"]
        self.cost = def_cost if safe["cost"] == None else safe["cost"]
        self.description = get_description(safe["description"])

    def getall(self):
        return [
                self.name,
                self.abtype,
                self.cost,
                self.description
                ]

    def get_yaml(self):
        return copy.deepcopy({
                "name": self.name,
                "type": self.type,
                "cost": self.cost,
                "description": self.description
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Type: {self.type}\n" + \
               f"Cost: {self.cost} SP\n" + \
               f"Description: {self.description}"

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name < other.name
        elif self.type != other.type:
            return self.type < other.type
        elif self.cost != other.cost:
            return self.cost < other.cost
        else:
            return False

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name > other.name
        elif self.type != other.type:
            return self.type > other.type
        elif self.cost != other.cost:
            return self.cost > other.cost
        else:
            return False

    def __ge__(self, other):
        return (self > other) or (self == other)

class Course:
    """
    Course class

    Properties:
        str name
        str description
    """
    def __init__(self, yaml_dict=None):
        safe = safe_dict(yaml_dict, "None")
        self.name = safe["name"]
        self.description = get_description(safe["description"])

    def getall(self):
        return [
                self.name,
                self.description
                ]

    def get_yaml(self):
        return copy.deepcopy({
                "name": self.name,
                "description": self.description
                })

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Description: {self.description}"

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name < other.name
        else:
            return False

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        if type(self) != type(other):
            raise TypeError
        elif self.name != other.name:
            return self.name > other.name
        else:
            return False

    def __ge__(self, other):
        return (self > other) or (self == other)

def object_list(yaml_list, constructor):
    """
    Return a list of objects created with the given constructor from a list of
    dictionaries returned by the yaml parser.
    """
    obj_list = list()
    if yaml_list != None:
        for obj in yaml_list:
            obj_list.append(constructor(obj))
    return obj_list

def prune_equip(obj_list):
    """
    Make sure only one of the objects in obj_list has the property `equip` set
    to True.
    """
    prune = False
    pruned = copy.copy(obj_list)
    for obj in pruned:
        try:
            val = obj.equip
        except:
            raise Exception("list contains an object which does not have property `equip`")
            return obj_list
        if obj.equip and prune:
            obj.equip = False
        elif obj.equip:
            prune = True
        else:
            pass
    return pruned

def load_char(charfile="char.yml"):
    """
    Loads a complete character sheet from a given .ini file.
    Assumes the correct format (lol)

    INPUTS:
        charfile - path to the .yml file relative to the executing file

    OUTPUTS: (charinfo, stats, chart, traits, abilities, inventory)
        charinfo  - CharInfo object
        stats     - Stats object
        chart     - Chart object
        traits    - [Trait objects]
        abilities - [Ability objects]
        inventory - [[Armor], [Shield], [Weapon], [Item], [Consumable], Crystals]
    """
    with open(charfile, 'r') as infile:
        char = safe_dict(yaml.load(infile))
    
    charinfo = CharInfo(char["general"])
    stats = Stats(char["stats"])
    chart = Chart(char["attributes"], char["aspects"], char["skills"])
    armors = prune_equip(object_list(char["armors"], Armor))
    shields = prune_equip(object_list(char["shields"], Shield))
    weapons = object_list(char["weapons"], Weapon)
    items = object_list(char["items"], Item)
    consumables = object_list(char["consumables"], Consumable)
    crystals = Crystals(char["crystals"])
    inventory = [armors, shields, weapons, items, consumables, crystals]
    traits = object_list(char["traits"], Trait)
    abilities = object_list(char["abilities"], Ability)

    return (charinfo, stats, chart, traits, abilities, inventory)

def save_char(allinfo, charfile="char.yml"):
    (charinfo, stats, chart, traits, abilities, inventory) = allinfo
    [armor, shield, weapons, items, consumables, crystals] = inventory

    output = dict()
    output["general"] = charinfo.get_yaml()
    output["stats"] = stats.get_yaml()
    (output["attributes"], output["aspects"], output["skills"]) = chart.get_yaml()
    output["traits"] = [trait.get_yaml() for trait in traits]
    output["abilities"] = [ability.get_yaml() for ability in abilities]
    output["armors"] = [armor.get_yaml() for armor in armors]
    output["shields"] = [shield.get_yaml() for shield in shields]
    output["weapons"] = [weapon.get_yaml() for weapon in weapons]
    output["items"] = [item.get_yaml() for item in items]
    output["consumables"] = [consumable.get_yaml() for consumable in consumables]
    output["crystals"] = crystals.get_yaml()

    with open(charfile, 'w') as outfile:
        yaml.dump(output, outfile)

def example_char():
    charinfo = CharInfo()
    stats = Stats()
    chart = Chart()
    traits = [Trait()]
    abilities = [Ability()]
    inventory = [
            [Armor()],
            [Shield()],
            [Weapon()],
            [Item()],
            [Consumable()],
            Crystals()
        ]
    return (charinfo, stats, chart, traits, abilities, inventory)

def sort_inventory():
    global traits, abilities, armors, shields, weapons, items, consumables

    traits.sort()
    abilities.sort()
    armors.sort()
    shields.sort()
    weapons.sort()
    items.sort()
    consumables.sort()

#------------------------------------------------------------------------------#

helptext = """
Character sheet manager for Adventure Academy characters. This script offers a
text-based terminal interface as well as a command-line interface. Requires curses.

Usage: charman.py [ options... ] [ charfile.yml ]

Arguments:
    charfile.yml
        Path to the `.yml` file containing character data. If omitted, the program
        will attempt to load `char.yml` in the current directory. If the specified
        file (or `char.yml` in the case of omission) does not exist, then a default
        character will be loaded.

Options:
    -c, --cli
        Run in command-line interface mode. In CLI mode, run `help` for more
        information.

    --generate-char
        Generate an example character file `char.yml` in the current directory.

    -h, --help
        Print this text.
"""

def less(stdscr, text):
    """
    Mimic the scrolling behavior of `less` with curses.
    """
    # make a new window over the given window, and assign it a panel to layer
    # over. this should maintain whatever's already on the terminal window
    (height, width) = stdscr.getmaxyx()
    (y0, x0) = stdscr.getbegyx()
    win = cu.newwin(height, width, y0, x0)
    helppan = pa.new_panel(win)
    helppan.top()

    # curses settings for processing keyboard input
    win.keypad(True)
    win.nodelay(False)
    cu.curs_set(0)
    win.scrollok(False)

    # get the text, and start by printing from the top
    lines = text.split('\n')
    h = 0
    hmax = len(lines)-1
    k = 0
    kmax = max([len(lines[i]) for i in range(len(lines))])-1

    while True:
        # refresh the recorded window dimensions
        (height, width) = win.getmaxyx()

        # print all the output that will fit in the window
        win.move(0,0)
        for line in lines[h:h+height-1]:
            win.addstr(f"{line[k:k+width]}\n")
        for i in range(height-1-(hmax-h+1)):
            win.addstr(pad_str('~', width-1)+'\n')
        win.addstr(" press q to quit ", cu.A_REVERSE)
        win.refresh()

        # process input from keyboard
        # up, k     ->  scroll up one line
        # down, j   ->  scroll down one line
        # left, h   ->  scroll left one line
        # right,l   ->  scroll right one line
        # K         ->  scroll up 20 lines
        # J         ->  scroll down 20 lines
        # pageup    ->  scroll up by one window height
        # pagedown  ->  scroll down by one window height
        # shift+left, H ->  scroll left 20 columns
        # shift+right,L ->  scroll right 20 columns
        # q         ->  quit
        ch = win.getch()
        if ch in [cu.KEY_UP, ord('k')]:
            h = max(0, h-1)
        elif ch in [cu.KEY_DOWN, ord('j')]:
            h = min(hmax, h+1)
        elif ch in [cu.KEY_LEFT, ord('h')]:
            k = max(0, k-1)
        elif ch in [cu.KEY_RIGHT, ord('l')]:
            k = min(kmax, k+1)
        elif ch in [ord('K')]:
            h = max(0, h-20)
        elif ch in [ord('J')]:
            h = min(hmax, h+20)
        elif ch in [cu.KEY_PPAGE]:
            h = max(0, h-height)
        elif ch in [cu.KEY_NPAGE]:
            h = min(hmax, h+height)
        elif ch in [cu.KEY_SLEFT, ord('H')]:
            k = max(0, k-20)
        elif ch in [cu.KEY_SRIGHT, ord('L')]:
            k = min(kmax, k+20)
        elif ch == ord('q'):
            # had some wonky behavior on windows before, this is untested
            # but should make the help screen turn invisible and return the user
            # to the original terminal window
            helppan.bottom()
            pa.update_panels()
            win.clear()
            win.refresh()
            del helppan
            del win
            #if sys.platform == "win32":
            #    stdscr.clear()
            return 0
        else:
            pass

def pad_str(s, l, j='l', c=' '):
    """
    Returns `s` as a string, padded with `c` characters to fill out a string of
    length `l`. If `l` is less than the length of `s` as a string, then `s` is
    cropped.
    """
    if l < len(str(s)):
        if j in ['r', 'right']:
            return str(s)[len(str(s))-l:]
        else:
            return str(s)[:l]
    elif j in ['c', 'center']:
        return ((l-len(str(s)))//2)*c + str(s) + int((l-len(str(s)))/2+0.5)*c
    elif j in ['r', 'right']:
        return (l-len(str(s)))*c + str(s)
    else:
        return str(s) + (l-len(str(s)))*c

def pad_multiline(s, pad, l=None, c=' '):
    """
    Returns string `s` as a string, padded on the left of each line with `pad`
    number of `c` characters. If any line is of length greater than `l`, then
    the line is cropped to fit.
    """
    lines = s.split('\n')
    outstr = ''
    length = max([len(line) for line in lines])+pad if l == None else l
    for line in lines:
        outstr = outstr + pad*c+line[:max(0, length-pad)]+'\n'
    return outstr[:-1]

def char_dict_pack():
    """
    Returns a dictionary of all the character objects along with their constructors.
    Keys do not include aliases.
    """
    global charinfo, stats, chart, traits, abilities
    global armors, shields, weapons, items, consumables, crystals

    objs = dict()
    objs["general"]     = [charinfo, CharInfo]
    objs["stats"]       = [stats, Stats]
    objs["chart"]       = [chart, Chart]
    objs["trait"]       = [traits, Trait]
    objs["ability"]     = [abilities, Ability]
    objs["armor"]       = [armors, Armor]
    objs["shield"]      = [shields, Shield]
    objs["weapon"]      = [weapons, Weapon]
    objs["item"]        = [items, Item]
    objs["consumable"]  = [consumables, Consumable]
    objs["crystal"]     = [crystals, Crystals]
    return objs

def char_dict_unpack(objs):
    """
    Sets appropriate global variables from the char dict.
    """
    global charinfo, stats, chart, traits, abilities, inventory
    global armors, shields, weapons, items, consumables, crystals

    charinfo    = objs["general"][0]
    stats       = objs["stats"][0]
    chart       = objs["chart"][0]
    traits      = objs["trait"][0]
    abilities   = objs["ability"][0]
    armors      = objs["armor"][0]
    shields     = objs["shield"][0]
    weapons     = objs["weapon"][0]
    items       = objs["item"][0]
    consumables = objs["consumable"][0]
    crystals    = objs["crystal"][0]
    inventory = [armors, shields, weapons, items, consumables, crystals]

def parse_prop(prop, val):
    """
    Returns the correct python object from `set` or `add` command user input.
    """
    # see the `properties` help text for info on expected values
    try:
        if prop in ["background", "description", "effect", "name", "race",
                    "status", "type"]:
            return str(val)
        elif prop in ["damage"]:
            if is_valid_dice(val):
                return str(val)
            else:
                raise Exception('invalid property value')
        elif prop in ["amount", "chart", "cost", "credits", "crystal", "defense",
                      "range", "speed", "term", "uses"]:
            return int(val)
        elif prop == "ammo":
            if val.lower() == "inf":
                return "Inf"
            else:
                return int(val)
        elif prop in ["durability"]:
            if val.lower() == "inf":
                return [m.inf, m.inf]
            elif ',' in val:
                val = val.split(',')
                curp = int(val[0])
                maxp = int(val[1])
                return [curp, maxp]
            else:
                return int(val)
        elif prop in ["hp", "cp", "sp", "mp", "uses"]:
            if ',' in val:
                val = val.split(',')
                curp = int(val[0])
                maxp = int(val[1])
                return [curp, maxp]
            else:
                return int(val)
        elif prop == "size":
            try:
                val = int(val)
            except:
                pass
            return get_size(val)
        elif prop == "weight":
            try:
                val = int(val)
            except:
                pass
            return get_weight(val)
        elif prop in ["equip", "inspiration"]:
            if val.lower() in ['t', 'true']:
                return True
            elif val.lower() in ['f', 'false']:
                return False
            else:
                raise Exception('invalid property value')
    except:
        raise Exception('invalid property value')

def get_input(stdscr, history, prompt, cliflag=True, def_in=''):
    """
    Returns the user's input as a string

    Args:
        stdscr - curses window
        history - string list of previous user inputs
        prompt - string given the text to display to the left of the user's input
        cliflag - bool determining whether the program is running in CLI mode
        def_in - default 'starting' input for the user

    Returns:
        com - string containing user input
        hist - history of user inputs including the most recent
    """
    # initialize operating variables
    p = len(prompt)
    hist = copy.copy(history)

    # allow for the ability to start the user with some base input
    if '' not in hist[:1]:
        hist.insert(0, '')
    hist[0] = def_in

    # define initial position
    h = 0   # history item
    j = p   # cursor position in the window
    k = 0   # scrolling offset in the history item
    com = hist[0]   # current user input
    while True: # process each key pushed by the user
        # refresh window size and initial cursor position
        (height, width) = stdscr.getmaxyx()
        (y0, x0) = stdscr.getyx()

        # print the prompt and as much of the current user input as will fit on
        # one line
        stdscr.addstr(prompt)
        for i in range(width-1-p):
            stdscr.addstr(" ")
        stdscr.move(y0, p)
        displaystr = com[k:k+width-1-p]
        stdscr.addstr(displaystr)
        
        # move to the correct position in the window
        stdscr.move(y0, j)
        stdscr.refresh()

        ch = stdscr.getch()
        # go back one item in the history and set the cursor position to the end
        # of that string
        if ch == cu.KEY_UP:
            hp = h+1
            if hp < len(hist):
                hist[0] = com if h == 0 else hist[0]
                h = hp
                com = hist[h]
                j = min(len(com)+p, width-1)
                k = max(0, len(com) - width - p)

        # go forward one item in the history and set the cursor position to the
        # end of that string
        elif ch == cu.KEY_DOWN:
            hp = h-1
            if hp >= 0:
                h = hp
                com = hist[h]
                j = min(len(com)+p, width-1)
                k = max(0, len(com) - width - p)

        # move the cursor one space to the right unless it's at the end of the
        # window, in which case scroll the command to the right if possible
        elif ch == cu.KEY_RIGHT:
            jp = j + 1
            if jp - p + k > len(com):
                pass
            elif jp >= width:
                k = k + 1
            else:
                j = jp

        # move the cursor one space to the left unless it's right up against the
        # prompt, in which case scroll the command to the left if possible
        elif ch == cu.KEY_LEFT:
            jp = j - 1
            if jp - p + k < 0:
                pass
            elif jp < p:
                k = k - 1
            else:
                j = jp

        # remove the character to the left of the cursor and move the cursor
        # accordingly
        elif ch in [cu.KEY_BACKSPACE, asc.BS]:
            com = com[:max(0, j-p-k-1)]+com[j-p+k:]
            jp = j - 1
            kp = k - 1
            if jp - p + k < 0:
                pass
            elif jp < p:
                k = k - 1
            elif kp < 0:
                j = jp
            else:
                k = kp

        # remove the character to the right of the cursor (the one it's highlighting
        # if that's how your terminal looks
        elif ch in [cu.KEY_DC, asc.DEL]:
            com = com[:max(0, j-p-k)]+com[j-p+k+1:]

        # move the cursor as far left on the screen and in the command as possible
        elif ch in [cu.KEY_HOME]:
            j = p
            k = 0

        # move the cursor as far right on the screen and in the command as possible
        elif ch in [cu.KEY_END]:
            j = min(len(com)+p, width-1)
            k = max(0, len(com)-(width-1)-p)

        # enter the command in the history and return both after adjusting the
        # window and cursor to get it ready for the next time user input is needed
        elif ch in [ord('\n'), cu.KEY_ENTER]:
            #stdscr.move(y0, width-1)
            if y0+1 < height:
                stdscr.move(y0+1, x0)
            else:
                stdscr.scroll()
                stdscr.move(y0, x0)
            hist[0] = com
            return (com, hist)

        # if not in the cli, get the window and cursor ready for the next time
        # user input is needed, but return the special `escape` command and history
        elif ch in [asc.ESC]:
            if not cliflag:
                if y0+1 < height:
                    stdscr.move(y0+1, x0)
                else:
                    stdscr.scroll()
                    stdscr.move(y0,x0)
                hist[0] = com
                return ('escape', hist)

        # if not a special character, enter it into the command to the right of
        # the cursor and move it accordingly
        else:
            com = com[:j-p+k]+chr(ch)+com[j-p+k:]
            jp = j + 1
            if jp - p + k > len(com):
                pass
            elif jp >= width:
                k = k + 1
            else:
                j = jp

        stdscr.move(y0, x0)

def exec_command(stdscr, command, cliflag=True):
    """
    Execute a command string returned by `user_input`
    """
    global charinfo, stats, chart, traits, abilities, inventory
    global armors, shields, weapons, items, consumables, crystals
    global charfile

    # kind of unfortunate, but I can't think of another way to store this info
    # without causing clutter somewhere, and without loading from another file
    helptext = dict()
    helptext["default"] = """
Available commands: roll, load, save, get, set, add, remove, use, rank, morale,
    clear, quit, help, man

See `help <command>` (or `man <command>` if your screen is small) for more information.
In the text for an individual command, square brackets ([]) denote optional fields
while angled brackets (<>) denote required fields, and vertical line characters
(|) separate possible field arguments. Also see `help properties`, `help aliases`,
and `help objects`.
    """
    helptext["roll"] = """
Evaluate a given dice expression or expressions.
Usage:  roll [ options... ] expr1 expr2 ...
Args:
    expr1 expr2 ...
        Dice expressions. Valid expressions take the form `ndk[+/-m]` where n is
        the number of dice, k is the number of sides, and +/-m is the optional
        modifier.
        Expressions can also be attribute/aspect/skill names. Such an expression
        is evaluated as `1d20+m`, where `m` is the correct modifier.
Options:
    -a, --advantage
        Roll only the first given dice expression with advantage.
    -d, --disadvantage
        Roll only the first given dice expression with disadvantage.
    -n <int>
        Specify the number of times to evaluate the first given dice expression
        for an advantage/disadvantage roll.
    -m
        Add morale to the roll.
    -w <idx>
        Roll a weapon's damage.
Aliases:
    r           --->    roll
    roll+, r+   --->    roll -a
    roll-, r-   --->    roll -d
    """
    helptext["quit"] = """
Exit the program.
Usage:  quit
Aliases:
    q       --->    quit
    """
    helptext["load"] = """
Load data from a character sheet file. Note: this command sets the default character
file (originally the character file specified by the launch from the shell) used
by other commands like `save` for the current session.
Usage:  load <charfile.yml>
Args:
    charfile.yml
        Path to a character sheet .yml file.
    """
    helptext["save"] = """
Save all the current character data to a file. Note: this command sets the default
character file (originally the character file specified by the launch from the shell)
used in future calls of `save` for the current session.
Usage: save [ charfile.yml ]
args:
    charfile.yml
        Path to a character sheet .yml file. If omitted, data is saved to the
        original character file specified in the launch from the shell.
    """
    helptext["get"] = """
Print the property or properties of a character object.
Usage:  get <general|stats> [ property ]
        get chart [ -m ] [ position ]
        get crystal [ color ] [ size ]
        get <trait|ability|armor|shield|weapon|item|consumable> [ index ] [ property ]
Args:
    general|stats|chart|trait|ability|armor|shield|weapon|item|consumable|crystal
        Identifier for what sort of object you want info on.
    property
        Name of the property. If omitted, print all properties of the object. If
        included, must be a valid property of the object as returned by the omitted
        case. See `help objects` for info on what objects comprise which properties.
    position
        Position of the cell in the chart. Can be either the i,j position (with
        0,0 being the upper-left corner) of a cell on the chart or the cell's
        name.
    color
        Color of the type of crystal desired. If omitted, print all colors and sizes.
        Must be one of:
            black, red, orange, yellow, green, blue, violet, white, clear
    size
        Size of the type of crystal desired. If omitted, print all sizes of the
        given color. Must be one of:
            small, medium, large, extra
    index
        If omitted, print the names of all such objects, along with their indexing
        numbers. If included, must be an integer within the range of indices returned
        by the omitted case.
Options:
    -m
        Print the total modifier of the chart cell instead of the raw effort or
        mod-adjustment.
Aliases:
    g       --->    get             bk      --->    black
    ge      --->    general         re      --->    red
    st      --->    stats           or      --->    orange
    ch      --->    chart           ye      --->    yellow
    tr      --->    trait           gr      --->    green
    ab      --->    ability         bl      --->    blue
    ar      --->    armor           vi      --->    violet
    sh      --->    shield          wh      --->    white
    we      --->    weapon          cl      --->    clear
    it      --->    item            s       --->    small
    co      --->    consumable      m       --->    medium
    cr      --->    crystal         l       --->    large
                                    x       --->    extra
    """
    helptext["set"] = """
Set a property of a character object.
Usage:  set <general|stats> <property> <val>
        set chart <position> <val>
        set crystal <color> <size> <val>
        set <trait|ability|armor|shield|weapon|item|consumable> <index> <property> <val>
Args:
    general|stats|chart|trait|ability|armor|shield|weapon|item|consumable|crystal
        Identifier for what sort of object whose property value you want to set.
    property
        Name of the property. Must be a valid property of the object as returned
        by the omitted case of the corresponding `get` command. See `help objects`
        for info on what objects comprise which properties.
    position
        Position of the cell in the chart. Can be either the i,j position (with
        0,0 being the upper-left corner) of a cell on the chart or the cell's
        name.
    color
        Color of the type of crystal desired. Must be one of:
            black, red, orange, yellow, green, blue, violet, white, clear
    size
        Size of the type of crystal desired. Must be one of:
            small, medium, large, extra
    index
        Index of the object. Must be an integer within the range of indices returned
        by the omitted case of the corresponding `get` command.
    val
        The new value for the given property. See `help properties` for info on
        expected property values.
Options:
    -a
        Add `val` to a property's value instead of setting it.
    -z
        Subtract `val` from a property's value instead of setting it. `val` in this
        case should not be a string.
Aliases:
    s       --->    set             bk      --->    black
    ge      --->    general         re      --->    red
    st      --->    stats           or      --->    orange
    ch      --->    chart           ye      --->    yellow
    tr      --->    trait           gr      --->    green
    ab      --->    ability         bl      --->    blue
    ar      --->    armor           vi      --->    violet
    sh      --->    shield          wh      --->    white
    we      --->    weapon          cl      --->    clear
    it      --->    item            s       --->    small
    co      --->    consumable      m       --->    medium
    cr      --->    crystal         l       --->    large
                                    x       --->    extra
    """
    helptext["properties"] = """
This text gives info on the expected types of and default settings for all possible
values given in `set` and `add` commands.
    PROPERTY                TYPE                        DEFAULT VALUE
    ========                ====                        =============
    ammo                    int or 'inf'                0
    amount                  int                         1
    background              string                      'None'
    chart cell              int                         0
    cost                    int                         0
    credits                 int                         0
    crystal amount          int                         0
    damage                  dice expression             '1d0+0'
    defense                 int                         0
    description             string                      'None'
    durability              int or int,int or 'inf'     0,0
    effect                  string                      'None'
    equip                   bool                        False
    hp,cp,sp,mp             int or int,int              1,1
    inspiration             bool                        False
    name                    string                      'None'
    race                    string                      'None'
    range                   int                         0
    size                    string or int               'None'
    speed                   int                         1
    status                  string                      'Healthy'
    term                    int                         1
    type                    string                      'None'
    uses                    int or int,int              1,1
    weight                  string or int               'None'
    """
    helptext["objects"] = """
This text gives info on which objects comprise which properties for use in `get`
and `set` commands.
    OBJECT      PROPERTIES          OBJECT      PROPERTIES
    =====       ==========          ======      ==========
    general     name                armor       name       
                race                            defense    
                background                      weight     
                size                            durability 
                speed                           effect     
                term                            description
                credits                         equip      
                inspiration         ---------------------- 
    ----------------------          shield      name       
    stats       hp                              defense    
                cp                              weight     
                sp                              durability 
                mp                              effect     
                status                          description
    ----------------------                      equip      
    trait       name                ---------------------- 
                type                weapon      name       
                description                     damage     
    ----------------------                      type       
    ability     name                            range      
                type                            ammo       
                cost                            weight     
                description                     durability 
    ----------------------                      effect     
    consumable  name                            description
                uses                ---------------------- 
                amount              item        name       
                description                     description
    """
    helptext["aliases"] = """
This text lists all the available command and field aliases.
    COMMANDS                        FIELDS
    ========                        ======
    r           --->    roll        ge      --->    general
    roll+, r+   --->    roll -a     st      --->    stats
    roll-, r-   --->    roll -d     ch      --->    chart
    g           --->    get         tr      --->    trait
    s           --->    set         ab      --->    ability
    a           --->    add         ar      --->    armor
    rm          --->    remove      sh      --->    shield
    u           --->    use         we      --->    weapon
    eq          --->    equip       it      --->    item
    ueq         --->    unequip     co      --->    consumable
    q           --->    quit        cr      --->    crystal
    h           --->    help        bk      --->    black
                                    re      --->    red
                                    or      --->    orange
                                    ye      --->    yellow
                                    gr      --->    green
                                    bl      --->    blue
                                    vi      --->    violet
                                    wh      --->    white
                                    cl      --->    clear
                                    s       --->    small
                                    m       --->    medium
                                    l       --->    large
                                    x       --->    extra
    """

    helptext["add"] = """
Add a new trait, ability, armor, shield, weapon, item, or consumable to the current
inventory. Usage of this command prompts a small walkthrough to set each property
of the new object. If any user inputs in this walkthrough are left blank, then a
default value is assumed (see `help properties`). The walkthrough can be canceled
at any time by entering `/cancel` for any property. If the walkthrough is canceled,
no new object is added to the inventory.
Usage:  add <trait|ability|armor|shield|weapon|item|consumable> [ index ]
Args:
    trait|ability|armor|shield|weapon|item|consumable
        Identifier for what sort of object you want to add to the current inventory.
    index
        Index at which the new object will be inserted.
Aliases:
    a       --->    add
    tr      --->    trait
    ab      --->    ability
    ar      --->    armor
    sh      --->    shield
    we      --->    weapon
    it      --->    item
    co      --->    consumable
    """
    helptext["remove"] = """
Remove a trait, ability, armor, shield, weapon, item, or consumable from the current
inventory. Removed objects are deleted on the spot after confirmation, so use this
carefully. It's a good idea to use `get` to make sure your index is correct before
using this command.
Usage:  remove <trait|ability|armor|shield|weapon|item|consumable> <index>
Args:
    trait|ability|armor|shield|weapon|item|consumable
        Identifier for what sort of object you want to remove from the current
        inventory.
    index
        Index of the object. Must be within the range of indices returned by the
        corresponding `get` command.
Aliases:
    rm      --->    remove
    tr      --->    trait
    ab      --->    ability
    ar      --->    armor
    sh      --->    shield
    we      --->    weapon
    it      --->    item
    co      --->    consumable
    """
    helptext["use"] = """
Decrement the `uses` property of a given consumable by an amount. If the new
`uses` property is less than 1, overflow is accounted for. If the new `uses`
property is greater than 0, a new consumable object is spawned with the correct
number of uses, while the old object's amount is decremented.
Usage:  use <index> [ n ]
Args:
    index
        Index of the consumable object. Must be within the range of indices
        returned by the corresponding `get` command.
    n
        Integer number of times the consumable is to be used. Defaults to 1 if
        unspecified.
Aliases:
    u       --->    use
    """
    helptext["equip"] = """
Mark an armor or shield object as equipped. Doing so marks all other armor or
shield objects as unequipped.
Usage:  equip <armor|shield> <index>
Args:
    armor|shield
        Identifier for what sort of object you want to equip.
    index
        Index of the armor or shield object. Must be within the range of indices
        returned by the corresponding `get` command.
Aliases:
    eq      --->    equip
    ar      --->    armor
    sh      --->    shield
    """
    helptext["unequip"] = """
Mark the currently equipped armor or shield as unequipped. No effect if no armor
or shield is currently equipped.
Usage:  unequip <armor|shield>
Args:
    armor|shield
        Identifier for what sort of object you want to unequip
Aliases:
    ueq     --->    unequip
    ar      --->    armor
    sh      --->    shield
    """
    helptext["rank"] = """
Compute rank for a given effort number.
Usage:  rank <eff>
Args:
    eff
        Effort value
    """
    helptext["morale"] = """
Compute morale for a given composure (CP) number.
Usage:  morale [ comp ]
Args:
    comp
        Composure (CP) value. If unspecified, computes morale from the character's
        current cp value.
    """
    helptext["clear"] = """
Clean the screen.
Usage:  clear
    """
    helptext["help"] = """
Display available commands or additional information about a specific command.
Usage:  help [ command ]
Aliases:
    h       --->    help
    """
    helptext["man"] = """
Display help text in a less-like mini-environment.
Usage: man <command>
    """

    aliases = {
            "ge":           "general",
            "st":           "stats",
            "ch":           "chart",
            "tr":           "trait",
            "ab":           "ability",
            "ar":           "armor",
            "sh":           "shield",
            "we":           "weapon",
            "it":           "item",
            "co":           "consumable",
            "cr":           "crystal",
            "traits":       "trait",
            "abilities":    "ability",
            "armors":       "armor",
            "shields":      "shield",
            "weapons":      "weapon",
            "items":        "item",
            "consumables":  "consumable",
            "crystals":     "crystal",
            "bk":           "black",
            "re":           "red",
            "or":           "orange",
            "ye":           "yellow",
            "gr":           "green",
            "bl":           "blue",
            "vi":           "violet",
            "wh":           "white",
            "cl":           "clear",
            "small":        "s",
            "medium":       "m",
            "large":        "l",
            "extra":        "x"
        }
    command = command.split(" ") 
    if command[0] in ['']:
        return 0

    ######## roll ########
    elif command[0] in ["roll", "r", "roll+", "r+", "roll-", "r-"]:
        # parse options, args
        shortopts = "adn:mw:"
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"roll: {e}\n")
            return 0
        config = dict()
        w = False # roll a weapon damage instead
        if command[0] in ["roll+", "r+"]:
            config["adv"] = "+"
        elif command[0] in ["roll-", "r-"]:
            config["adv"] = "-"
        for opt, optarg in opts:
            if opt in ["-a"]:
                config["adv"] = "+"
            elif opt in ["-d"]:
                config["adv"] = "-"
            elif opt in ["-n"]:
                try:
                    config["n"] = int(optarg)
                except Exception as e:
                    stdscr.addstr(f"roll -n: {e}\n")
                    return 0
            elif opt in ["-m"]:
                config["morale"] = morale(stats.cp[0])
            elif opt in ["-w"]:
                try:
                    widx = int(optarg)
                    dmg_dice = weapons[widx].damage
                    args = [dmg_dice]
                    w = True
                except Exception as e:
                    stdscr.addstr(f"roll -w: invalid argument\n")
                    return 0

        # convert all arguments to proper dice expressions if possible
        rolls_disp = copy.copy(args)
        for i in range(len(args)):
            if not is_valid_dice(args[i]):
                try:
                    mod = chart.getmod(args[i])
                    disp = chart.getname(args[i])
                    rolls_disp[i] = f"{disp} ({mod:+})"
                    args[i] = f"1d20{mod:+}"
                except Exception as e:
                    stdscr.addstr(f"roll: {e}\n")
                    return 0

        m = config.get("morale", 0)

        # roll with (dis)advantage
        if config.get("adv", False):

            # perform the roll
            n = config.get("n", 2)
            try:
                (rolls_ex, rolls_ex_m, totals, totals_m, all_rolls) = roll_ad(args[0], n, config["adv"])
            except Exception as e:
                stdscr.addstr(f"roll_ad: {e}\n")
                return 0

            # check whether all rolls comprise multiple groups of more than
            # one roll
            c = 1
            if w:
                stdscr.addstr(f"{weapons[widx].name}\n")
            #stdscr.addstr(" rolls:")
            stdscr.addstr(f"{rolls_disp[0]}:")
            for rolls in all_rolls:
                c = max(c, len(rolls))
                stdscr.addstr(f"  {rolls}")
            stdscr.addstr("\n")
            
            # if the individual roll groups produce more than one number,
            # print each of their totals
            if c > 1:
                stdscr.addstr("totals:")
                for total in totals:
                    stdscr.addstr(f"  {total}")
                stdscr.addstr("\n")

            # if the modifier is non-zero, print the mod-adjusted number as
            # well
            if totals_m != totals:
                stdscr.addstr("w/mods:")
                for total_m in totals_m:
                    stdscr.addstr(f"  {total_m+m}")
                stdscr.addstr("\n")

            # print the extreme value
            if config["adv"] == "+":
                stdscr.addstr(f"   max:  {rolls_ex_m+m}\n")
            elif config["adv"] == "-":
                stdscr.addstr(f"   min:  {rolls_ex_m+m}\n")
            return 0

        # regular roll
        else:
            # perform the roll
            try:
                (totals, totals_m, all_rolls) = multiroll(args)
            except Exception as e:
                stdscr.addstr(f"roll: {e}\n")
                return 0

            # if more than one dice expression is given, indent the lines
            # giving details on each roll
            offset = " " if len(args) > 1 else ""
            # print the numbers/totals/modified totals of each roll
            for i in range(len(args)):
                #if len(args) > 1:
                #    stdscr.addstr(f"{rolls_disp[i]}:")
                #else:
                #    stdscr.addstr("rolls:")
                if w:
                    stdscr.addstr(f"{weapons[widx].name}\n")
                stdscr.addstr(f"{rolls_disp[i]}:")
                for roll in all_rolls[i]:
                    stdscr.addstr(f"  {roll}")
                stdscr.addstr("\n")

                # if the expression rolls more than one die, print the total
                if len(all_rolls[i]) > 1:
                    stdscr.addstr(f"{offset}total:  {totals[i]}\n")

                # if the modifier is non-zero, print the mod-adjusted total
                if totals[i] != totals_m[i]:
                    stdscr.addstr(f"{offset}w/mod:  {totals_m[i]+m}\n")
            return 0

    ######## load ########
    elif command[0] in ["load"]:
        # parse options, args
        # try to load the given filename
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
            if len(args) > 0:
                charfile = args[0]
            else:
                stdscr.addstr("load: no character file specified\n")
                return 0
            (charinfop, statsp, chartp, traitsp, abilitiesp, inventoryp) = load_char(charfile)
            [armorsp, shieldsp, weaponsp, itemsp, consumablesp, crystalsp] = inventoryp
        except Exception as e:
            stdscr.addstr(f"load: {e}\n")
            return 0

        # set the global variables
        charinfo = charinfop
        stats = statsp
        chart = chartp
        traits = traitsp
        abilities = abilitiesp
        inventory = inventoryp
        armors = armorsp
        shields = shieldsp
        weapons = weaponsp
        items = itemsp
        consumables = consumablesp
        crystals = crystalsp
        stdscr.addstr(f"loaded character '{charinfo.name}' from '{charfile}'\n")
        return 0

    ######## save ########
    elif command[0] in ["save"]:
        # parse options, args
        # try to pack the global variables and write them to file
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
            if len(args) > 0:
                charfile = args[0]
            inventory = [armors, shields, weapons, items, consumables, crystals]
            allinfo = (charinfo, stats, chart, traits, abilities, inventory)
            save_char(allinfo, charfile)
        except Exception as e:
            stdscr.addstr(f"save: {e}\n")
            return 0
        stdscr.addstr(f"saved character '{charinfo.name}' to '{charfile}'\n")
        return 0

    ######## get ########
    elif command[0] in ["get", "g"]:
        # parse options, args
        shortopts = "m"
        getmod = False
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
            for opt, optarg in opts:
                if opt in ["-m"]:
                    getmod = True
        except Exception as e:
            stdscr.addstr(f"get: {e}\n")
            return 0

        if len(args) < 1:
            stdscr.addstr("get: object identifier unspecified\n")
            return 0

        # account for optional arguments
        args = args + ['', '']
        for i in range(len(args)):
            if args[i] in aliases.keys():
                args[i] = aliases[args[i]]

        # assemble the global variables
        objs = char_dict_pack()

        # non-iterable objects
        if args[0] in ["general", "stats"]:
            # get the object itself
            obj = objs[args[0]][0]
            
            # if a property is not specified, use the built-in __str__ method
            if args[1] == '':
                stdscr.addstr(f"{obj}\n")
                return 0

            # otherwise, try to get the value of the property
            else:
                obj = obj.get_yaml()
                try:
                    propval = obj[args[1].lower()]
                    stdscr.addstr(f"{propval}\n")
                    return 0
                except:
                    stdscr.addstr(f"get: invalid property '{args[1]}'\n")
                    return 0

        # iterable objects
        elif args[0] in ["trait", "ability", "weapon", "item", "consumable", "armor", "shield"]:
            # get the list of objects
            objlist = objs[args[0]][0]

            # print the entire list if index is unspecified
            if args[1] == '':
                # determine the proper padding for when indices are printed
                idxwidth = len(str(len(objlist)-1))
                for i in range(len(objlist)):
                    idx = pad_str(str(i), idxwidth, j='r')

                    # if the object has an `equip` property, print the equipped
                    # one bolded
                    try:
                        if objlist[i].equip == True:
                            stdscr.addstr(f"{idx}:  {objlist[i].name}\n", cu.A_BOLD)
                        else:
                            stdscr.addstr(f"{idx}:  {objlist[i].name}\n")
                    except AttributeError:
                        stdscr.addstr(f"{idx}:  {objlist[i].name}\n")
                return 0

            # if index is specified
            else:
                # try to get the object itself
                try:
                    obj = objlist[int(args[1])]
                except:
                    stdscr.addstr("get: invalid object index\n")
                    return 0

                # if no property is specified, print all properties using the
                # built-in __str__ method
                if args[2] == '':
                    stdscr.addstr(f"{obj}\n")
                    return 0

                # otherwise, print the property value if it's a valid property
                else:
                    obj = obj.get_yaml()
                    try:
                        propval = obj[args[2].lower()]
                        stdscr.addstr(f"{propval}\n")
                        return 0
                    except:
                        stdscr.addstr(f"get: invalid property '{args[1]}'\n")
                        return 0

        # special case 1
        elif args[0] in ["chart"]:
            # if position is unspecified, print the whole chart
            if args[1] == '':
                if getmod:
                    stdscr.addstr(f"{chart.modstr()}\n")
                    return 0
                else:
                    stdscr.addstr(f"{chart}\n")
                    return 0

            # otherwise, try to parse a position or cellname
            else:
                try:
                    if ',' in args[1]:
                        pos = args[1].split(',')
                        i = int(pos[0])
                        j = int(pos[1])
                    else:
                        (i,j) = chart.getpos(args[1].lower())

                    # print the cell
                    if getmod:
                        stdscr.addstr(f"{chart.names[i][j]}: {chart.getmod((i,j)):+}\n")
                        return 0
                    else:
                        stdscr.addstr(f"{chart.names[i][j]}: {chart[i,j]:+}\n")
                        return 0
                except Exception as e:
                    stdscr.addstr(f"get: {e}\n")
                    return 0

        # special case 2
        elif args[0] in ["crystal"]:
            # if a color is unspecified, print all crystals
            if args[1] == '':
                stdscr.addstr(f"{crystals}\n")
                return 0

            # if a size is unspecified, print all sizes
            elif args[2] == '':
                color = args[1].lower()
                if color in aliases.keys():
                    color = aliases[color]
                try:
                    sizes = crystals.getcolor(color)
                    for i in range(len(sizes)):
                        stdscr.addstr(f"{crystals.sizes[i].capitalize()}: {sizes[i]}   ")
                    stdscr.addstr('\n')
                    return 0
                except:
                    stdscr.addstr("get: invalid crystal specification\n")
                    return 0

            # if a specific color/size is specified, print it if possible
            else:
                color = args[1].lower()
                size = args[2].lower()
                if color in aliases.keys():
                    color = aliases[color]
                if size in aliases.keys():
                    size = aliases[size]
                try:
                    (i,j) = crystals.getpos(color, size)
                    color = crystals.colors[i].capitalize()
                    size = crystals.sizes[j].capitalize()
                    stdscr.addstr(f"{color} {size}: {crystals[i,j]}\n")
                    return 0
                except:
                    stdscr.addstr(f"get: invalid crystal specification\n")
                    return 0
        else:
            stdscr.addstr(f"get: invalid identifier '{args[0]}'\n")
            return 0

    ######## set ########
    elif command[0] in ["set", "s"]:
        # parse options, args
        shortopts = "a"
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"set: {e}\n")
            return 0
        config = dict()
        for opt, optarg in opts:
            if opt in ["-a"]:
                config["add"] = 1
            elif opt in ["-z"]:
                config["add"] = -1

        if len(args) < 1:
            stdscr.addstr("get: object identifier unspecified\n")
            return 0

        if args[0] in aliases.keys():
            args[0] = aliases[args[0]]

        # assemble the global variables
        objs = char_dict_pack()

        # non-iterable objects
        if args[0] in ["general", "stats"]:
            # make sure all positional arguments are specified
            if len(args) < 3:
                stdscr.addstr(f"set: expected 3 arguments, but got {len(args)}\n")
                return 0

            # concatenate all arguments beyond the second to account for strings
            # with spaces
            val = ""
            for i in range(2, len(args)):
                val = val + args[i]
                if i < len(args)-1:
                    val = val + " "

            # get the object itself and check that the property is valid
            obj = objs[args[0]][0].get_yaml()
            constr = objs[args[0]][1]
            propname = args[1].lower()
            if propname not in obj.keys():
                stdscr.addstr(f"set: invalid property '{args[1]}'\n")
                return 0

            # set/add the specified value, accounting for hp/sp/cp/mp special cases
            try:
                val = parse_prop(propname, val)
                if propname in ["hp", "sp", "cp", "mp"] and type(val) == type(int()):
                    if config.get("add", False):
                        obj[propname] = [obj[propname][0]+config["add"]*val, obj[propname][1]]
                    else:
                        obj[propname] = [val, obj[propname][1]]
                else:
                    if config.get("add", False):
                        obj[propname] = [obj[propname][0]+config["add"]*val[0],
                                obj[propname][1]+config["add"]*val[1]]
                    else:
                        obj[propname] = val

                # replace the object with a new one containing the right values
                objs[args[0]][0] = constr(obj)
                char_dict_unpack(objs)
                return 0
            except Exception as e:
                stdscr.addstr(f"set: {e}\n")
                return 0

        # iterable objects
        elif args[0] in ["trait", "ability", "weapon", "item", "consumable", "armor", "shield"]:
            # make sure all positional arguments are specified
            if len(args) < 4:
                stdscr.addstr(f"set: expected 4 arguments, but got {len(args)}\n")
                return 0

            # concatenate all arguments beyond the second to account for strings
            # with spaces
            val = ""
            for i in range(3, len(args)):
                val = val + args[i]
                if i < len(args)-1:
                    val = val + " "

            # get the object at the specified index
            try:
                idx = int(args[1])
                obj = objs[args[0]][0][idx].get_yaml()
                constr = objs[args[0]][1]
            except:
                stdscr.addstr(f"set: invalid index '{args[1]}'\n")
                return 0

            # try to set/add the specified value, accounting for dur/uses special
            # cases
            try:
                propname = args[2].lower()
                if propname not in obj.keys():
                    stdscr.addstr(f"set: invalid property '{args[2]}'\n")
                    return 0
                val = parse_prop(propname, val)
                if propname in ["durability", "uses"] and type(val) == type(int()):
                    if config.get("add", False):
                        obj[propname] = [obj[propname][0]+config["add"]*val, obj[propname][1]]
                    else:
                        obj[propname] = [val, obj[propname][1]]
                else:
                    if config.get("add", False):
                        obj[propname] = [obj[propname][0]+config["add"]*val[0],
                                obj[propname][1]+config["add"]*val[1]]
                    else:
                        obj[propname] = val
                objs[args[0]][0][idx] = constr(obj)

                # if `equip` had been set, make sure that only one of that object
                # is equipped
                if propname == "equip" and objs[args[0]][0][idx].equip:
                    for i in range(len(objs[args[0]][0][i])):
                        if objs[args[0]][0][i] and i != idx:
                            objs[args[0]][0][i].equip = False

                # replace the object with a new one containing the right values
                char_dict_unpack(objs)
                sort_inventory()
                return 0
            except Exception as e:
                stdscr.addstr(f"set: {e}\n")
                return 0

        # special case 1
        elif args[0] in ["chart"]:
            # ensure 3 arguments
            if len(args) < 3:
                stdscr.addstr(f"set: expected 3 arguments, but got {len(args)}\n")
                return 0

            # no need for concatenation
            val = args[2]

            # try to parse the chart position
            try:
                if ',' in args[1]:
                    pos = args[1].split(',')
                    i = int(pos[0])
                    j = int(pos[1])
                else:
                    (i,j) = chart.getpos(args[1].lower())

                # set/add the raw value only (-m doesn't make sense)
                val = parse_prop("chart", val)
                if config.get("add", False):
                    chart[i,j] = chart[i,j] + config["add"]*val
                else:
                    chart[i,j] = val
                return 0
            except Exception as e:
                stdscr.addstr(f"set: {e}\n")
                return 0

        # special case 2
        elif args[0] in ["crystal"]:
            # ensure 4 arguments
            if len(args) < 4:
                stdscr.addstr(f"set: expected 4 arguments but got {len(args)}\n")
                return 0

            # no need to concatenate
            val = args[3]

            # interpret color/size
            color = args[1].lower()
            size = args[2].lower()
            if color in aliases.keys():
                color = aliases[color]
            if size in aliases.keys():
                size = aliases[size]

            # get the internal position in the crystals object
            try:
                (i,j) = crystals.getpos(color, size)
                val = parse_prop("crystal", val)

                # set/add the value
                if config.get("add", False):
                    crystals[i,j] = crystals[i,j] + config["add"]*val
                else:
                    crystals[i,j] = val
                return 0
            except:
                stdscr.addstr(f"set: invalid crystal specification\n")
                return 0
        else:
            stdscr.addstr(f"set: invalid identifier '{args[0]}'\n")
            return 0

    ######## add ########
    elif command[0] in ["add", "a"]:
        # parse options, args
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"add: {e}\n")
            return 0
        if len(args) < 1:
            stdscr.addstr("add: object identifier unspecified\n")
            return 0

        # process aliases
        for i in range(len(args)):
            if args[i] in aliases.keys():
                args[i] = aliases[args[i]]

        # add only makes sense for iterable objects
        if args[0] in ["trait", "ability", "armor", "shield", "weapon", "item", "consumable"]:
            # assemble the global variables and make a new object
            objs = char_dict_pack()
            constr = objs[args[0]][1]
            newobj = constr().get_yaml()
            
            # account for optional index arguments
            args.append('')
            if args[1] == '':
                idx = len(objs[args[0]][0])
            else:
                try:
                    idx = int(args[1])
                except:
                    stdscr.addstr(f"invalid index '{args[1]}'\n")
                    return 0

            # get user input for each property
            add_hist = []
            for propname in newobj.keys():
                (propval, add_hist) = get_input(stdscr, add_hist, f"{propname}: ")
                if propval == '':
                    continue
                elif propval == "/cancel":
                    return 0

                # process the input and set it if valid
                try:
                    propval = parse_prop(propname, propval)
                    if type(propval) == type(int()):
                        if propname == "durability":
                            raise Exception("must specify both current and max values or 'inf'")
                        elif propname == "uses":
                            raise Exception("must specify both current and max values")
                except Exception as e:
                    stdscr.addstr(f"add: {e}\n")
                    return 0
                newobj[propname] = propval
                if propname == 'equip' and propval == True:
                    for obj in objs[args[0]][0]:
                        obj.equip = False

            # insert the new object into the global array
            objs[args[0]][0].insert(idx, constr(newobj))
            char_dict_unpack(objs)
            sort_inventory()
            return 0
        else:
            stdscr.addstr(f"add: invalid identifier '{args[0]}'\n")
            return 0

    ######## remove ########
    elif command[0] in ["remove", "rm"]:
        # parse options, args
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"remove: {e}\n")
            return 0

        if len(args) < 1:
            stdscr.addstr("remove: object identifier unspecified\n")
            return 0

        # process aliases
        for i in range(len(args)):
            if args[i] in aliases.keys():
                args[i] = aliases[args[i]]

        # remove only makes sense for iterable objects
        if args[0] in ["trait", "ability", "armor", "shield", "weapon", "item", "consumable"]:
            # assemble global variables
            objs = char_dict_pack()

            if len(args) < 2:
                stdscr.addstr(f"remove: expected 2 arguments, but got {len(args)}\n")
                return 0

            # check that the given index is valid
            try:
                idx = int(args[1])
                obj = objs[args[0]][0][idx]
            except:
                stdscr.addstr(f"remove: invalid index '{args[1]}'\n")
                return 0

            # ask for confirmation
            confirm = get_input(stdscr, [], f"confirm removal of '{obj.name}' [y,N]: ")[0].lower()
            if confirm in ['y', "yes"]:
                objs[args[0]][0].pop(idx)
                return 0
            else:
                return 0
        else:
            stdscr.addstr(f"remove: invalid identifier '{args[0]}'\n")
            return 0

    ######## use ########
    elif command[0] in ["use", "u"]:
        # parse options, args
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"use: {e}\n")
            return 0

        if len(args) < 1:
            stdscr.addstr(f"use: expected 1 argument, but got {len(args)}\n")
            return 0

        # check that the given index is valid
        try:
            idx = int(args[0])
            obj = consumables[idx]
            objdict = consumables[idx].get_yaml()
        except:
            stdscr.addstr(f"use: invalid index '{args[0]}'\n")
            return 0

        # account for optional argument, process the argument if specified
        if len(args) < 2:
            use = 1
        else:
            try:
                use = int(args[1])
                if use < 0:
                    raise Exception
            except:
                stdscr.addstr(f"use: invalid number of uses '{args[1]}'\n")
                return 0

        # compute new total uses
        uses_total = (obj.amount - 1)*obj.uses[1] + obj.uses[0] - use

        # subtract accordingly from uses and amount properties, spawning new
        # objects if necessary
        if uses_total < 1:
            consumables.remove(obj)
        elif uses_total % obj.uses[1] == 0:
            obj.uses[0] = obj.uses[1]
            obj.amount = int(uses_total/obj.uses[1])
        elif obj.amount > 1:
            obj.amount = int(uses_total/obj.uses[1])
            objdict["uses"][0] = objdict["uses"][1] - use%objdict["uses"][1]
            objdict["amount"] = 1
            consumables.insert(idx+1, Consumable(objdict))
        else:
            obj.uses[0] = obj.uses[0] - use
        sort_inventory()
        return 0

    ######## equip ########
    elif command[0] in ["equip", "eq"]:
        # parse options, args
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"equip: {e}\n")
            return 0

        if len(args) < 1:
            stdscr.addstr("equip: object identifier unspecified\n")
            return 0

        for i in range(len(args)):
            if args[i] in aliases.keys():
                args[i] = aliases[args[i]]

        # equip only makes sense for armors and shields
        if args[0] in ["armor", "shield"]:
            # assemble global variables
            objs = char_dict_pack()

            if len(args) < 2:
                stdscr.addstr(f"equip: expected 2 arguments, but got {len(args)}\n")
                return 0

            # verify that the given index is valid
            try:
                idx = int(args[1])
                obj = objs[args[0]][0][idx]
            except:
                stdscr.addstr(f"equip: invalid index '{args[1]}'\n")
                return 0

            # unequip everything else
            for i in range(len(objs[args[0]][0])):
                objs[args[0]][0][i].equip = False
            objs[args[0]][0][idx].equip = True

            # set global variables
            char_dict_unpack(objs)
            sort_inventory()
            return 0
        else:
            stdscr.addstr(f"equip: invalid identifier '{args[0]}'\n")
            return 0

    ######## unequip ########
    elif command[0] in ["unequip", "ueq"]:
        # parse options, args
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"unequip: {e}\n")
            return 0

        if len(args) < 1:
            stdscr.addstr("unequip object identifier unspecified\n")
            return 0

        for i in range(len(args)):
            if args[i] in aliases.keys():
                args[i] = aliases[args[i]]

        # unequip only makes sense for armors and shields
        if args[0] in ["armor", "shield"]:
            # assemble global variables
            objs = char_dict_pack()

            # set equip to false
            for i in range(len(objs[args[0]][0])):
                objs[args[0]][0][i].equip = False

            # set global variables
            char_dict_unpack(objs)
            return 0
        else:
            stdscr.addstr(f"unequip invalid identifier '{args[0]}'\n")
            return 0

    ######## rank ########
    elif command[0] in ["rank"]:
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"rank: {e}\n")
            return 0
        if len(args) < 1:
            stdscr.addstr("effort value unspecified\n")
            return 0
        try:
            eff = int(args[0])
        except:
            stdscr.addstr("invalid effort value\n")
            return 0
        stdscr.addstr(f"rank({eff}) = {rank(eff)}\n")
        return 0

    ######## morale ########
    elif command[0] in ["morale"]:
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"morale: {e}\n")
            return 0
        if len(args) < 1:
            cp = stats.cp[0]
        else:
            try:
                cp = int(args[0])
            except:
                stdscr.addstr("invalid composure value\n")
                return 0
        stdscr.addstr(f"morale({cp}) = {morale(cp)}\n")
        return 0

    ######## clear ########
    elif command[0] in ["clear"]:
        (y0, x0) = stdscr.getbegyx()
        stdscr.clear()
        stdscr.move(y0, x0)
        return 0

    ######## help ########
    elif command[0] in ["help", "h"]:
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"help: {e}\n")
            return 0
        if len(args) > 0:
            if args[0] in helptext.keys():
                stdscr.addstr(helptext[args[0]]+"\n")
            else:
                stdscr.addstr("command not found\n")
        else:
            stdscr.addstr(helptext["default"]+"\n")
        return 0

    ######## man ########
    elif command[0] in ["man", "manual"]:
        shortopts = ""
        try:
            opts, args = getopt.gnu_getopt(command[1:], shortopts)
        except Exception as e:
            stdscr.addstr(f"man: {e}\n")
            return 0
        if len(args) > 0:
            if args[0] in helptext.keys():
                #stdscr.addstr(helptext[args[0]]+"\n")
                less(stdscr, helptext[args[0]])
                stdscr.refresh()
                cu.curs_set(1)
            else:
                stdscr.addstr("command not found\n")
        else:
            stdscr.addstr(helptext["default"]+"\n")
        return 0

    ######## escape ########
    elif command[0] in ["escape"]:
        if not cliflag:
            return 1
        else:
            return 0

    ######## quit ########
    elif command[0] in ["quit", "q"]:
        return -1

    else:
        stdscr.addstr(f"Invalid command '{command[0]}'. See `help` for more details.\n")
        return 0

def cli_mode(stdscr, warns):
    """
    Run CLI mode. Prints all items in warns in the curses window.
    """
    global charinfo, stats, chart, traits, abilities, inventory
    global armors, shields, weapons, items, consumables, crystals
    global charfile

    cu.noecho()
    cu.start_color()
    cu.use_default_colors()
    stdscr.nodelay(False)
    cu.curs_set(1)
    stdscr.scrollok(True)
    stdscr.idlok(False)
    
    for warn in warns:
        stdscr.addstr(f"{warn}\n")
    stdscr.addstr(f"Loaded character '{charinfo.name}'\n")
    stdscr.refresh()

    history = list()
    while True:
        (command, history) = get_input(stdscr, history, ">> ")
        #(height, width) = stdscr.getmaxyx()
        #stdscr.addstr(pad_str(command, width-1)+"\n")
        code = exec_command(stdscr, command)
        if code < 0:
            return 0

def term_mode(stdscr, warns):
    """
    Run term mode. Prints all items in warns in the 'terminal' window.
    """
    global charinfo, stats, chart, traits, abilities, inventory
    global armors, shields, weapons, items, consumables, crystals
    global charfile

    cu.noecho()
    cu.start_color()
    cu.use_default_colors()
    stdscr.nodelay(False)
    cu.curs_set(0)
    stdscr.scrollok(False)
    stdscr.idlok(False)

    (height0, width0) = stdscr.getmaxyx()


    return 0

def main(argv):
    global charinfo, stats, chart, traits, abilities, inventory
    global armors, shields, weapons, items, consumables, crystals
    global charfile

    shortopts = "ch"
    longopts = [
            "cli",
            "generate-char",
            "help"
        ]
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
    except getopt.GetoptError as e:
        print("Getopt: "+str(e))
        sys.exit(1)

    config = dict()
    for opt, optarg in opts:
        if opt in ["-c", "--cli"]:
            config["cli"] = True
        elif opt in ["--generate-char"]:
            save_char(example_char(), "char.yml")
            return 0
        elif opt in ["-h", "--help"]:
            print(helptext)
            return 0
    warns = list()
    try:
        charfile = args[0]
    except:
        charfile = "char.yml"
        warns.append("Character file unspecified. Attempted to load 'char.yml'")
    try:
        (charinfo, stats, chart, traits, abilities, inventory) = load_char(charfile)
        [armors, shields, weapons, items, consumables, crystals] = inventory
    except:
        (charinfo, stats, chart, traits, abilities, inventory) = example_char()
        [armors, shields, weapons, items, consumables, crystals] = inventory
        warns.append("Unable to load character file. Using the default character instead.")
    sort_inventory()

    if config.get('cli', False):
        try:
            return cu.wrapper(cli_mode, warns)
        except KeyboardInterrupt:
            return 130
    else:
        print("The full terminal interface is not yet ready. Please launch with --cli.")
        return 1
        try:
            return cu.wrapper(term_mode, warns)
        except KeyboardInterrupt:
            return 130

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
