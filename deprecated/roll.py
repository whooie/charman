#!/usr/bin/python2

import random
import sys

def parse_command():
    user_in = raw_input(">> ")
    return user_in.split(' ')

def roll(ndn, mod=0):
    nums = ndn.split('d')
    try:
        dice = int(nums[0])
    except:
        print "Invalid argument."
        return
    sides = int(nums[1])

    tot = 0
    for i in range(dice):
        result = int(random.random()*sides + 1)
        sys.stdout.write("{:3}".format(result))
        tot = tot + result
    print ""

    print " nat: "+str(tot)
    if mod != 0:
        print " mod: "+str(tot+int(mod))

def rollperc():
    result = 10*int(random.random()*9)
    if result < 10:
        print "0"+str(result)
    else:
        print str(result)

#------------------------------------------------------------------------------#

ask = True
while ask:
    command = parse_command()
    if command[0] == "roll" or command[0] == "r":
        try:
            mod = command[2]
        except: 
            mod = 0
        roll(command[1], mod)
    elif command[0] == "rollp" or command[0] == "rollperc" or command[0] == "rp":
        rollperc()
    elif command[0] == "quit":
        ask = False
    else:
        print "Invalid command."
