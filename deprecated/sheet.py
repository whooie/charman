chart_aa = {\
        'strike':      ['str', 'offense'],\
        'aim':         ['dex', 'offense'],\
        'grapple':     ['con', 'offense'],\
        'force':       ['str', 'mobility'],\
        'sneak':       ['dex', 'mobility'],\
        'trek':        ['con', 'mobility'],\
        'parry':       ['str', 'defense'],\
        'evade':       ['dex', 'defense'],\
        'block':       ['con', 'defense'],\
        'charm':       ['cha', 'persuade'],\
        'negotiate':   ['emp', 'persuade'],\
        'aggro':       ['tem', 'persuade'],\
        'barter':      ['cha', 'diplomacy'],\
        'insight':     ['emp', 'diplomacy'],\
        'interrogate': ['tem', 'diplimacy'],\
        'mislead':     ['cha', 'deceive'],\
        'disguise':    ['emp', 'deceive'],\
        'suspect':     ['tem', 'deceive'],\
        'appraise':    ['int', 'craft'],\
        'medicine':    ['wis', 'craft'],\
        'engineer':    ['foc', 'craft'],\
        'investigate': ['int', 'awareness'],\
        'survive':     ['wis', 'awareness'],\
        'perceive':    ['foc', 'awareness'],\
        'knowledge':   ['int', 'acuity'],\
        'resolve':     ['wis', 'acuity'],\
        'concentrate': ['foc', 'acuity'],\
        }

chart_names = [\
        ['PHYSICAL',  'STR',         'DEX',       'CON'],\
        ['Offense',   'Strike',      'Aim',       'Grapple'],\
        ['Mobility',  'Force',       'Sneak',     'Trek'],\
        ['Defense',   'Parry',       'Evade',     'Block'],\
        ['SOCIAL',    'CHA',         'EMP',       'TEM'],\
        ['Persuade',  'Charm',       'Negotiate', 'Aggro'],\
        ['Diplomacy', 'Barter',      'Insight',   'Interrogate'],\
        ['Deceive',   'Mislead',     'Disguise',  'Suspect'],\
        ['MENTAL',    'INT',         'WIS',       'FOC'],\
        ['Craft',     'Appraise',    'Medicine',  'Engineer'],\
        ['Awareness', 'Investigate', 'Survive',   'Perceive'],\
        ['Acuity',    'Knowledge',   'Resolve',   'Concentrate'],\
        ['SPIRIT',    'Spellcast',   'Ritual',    'Artifact']
        ]

crystal_names = {\
        'black':  ['Black',  'Shadow',     'Necromancy'],\
        'red':    ['Red',    'Fire',       'Sylvamancy'],\
        'orange': ['Orange', 'Force',      'Augmentation'],\
        'yellow': ['Yellow', 'E&M',        'Abjuration'],\
        'green':  ['Green',  'Earth',      'Transmutation'],\
        'blue':   ['Blue',   'Water',      'Execration'],\
        'violet': ['Violet', 'Air',        'Illusion'],\
        'white':  ['White',  'Light',      'Emanation'],\
        'clear':  ['Clear',  'Compulsion', 'Divination']\
        }
