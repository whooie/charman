import math as m
import random
from collections import defaultdict as ddict
import yaml

def rank(eff):
    """
    Calculate rank given effort
    """
    return 0 if eff == 0 else int(eff/m.fabs(eff) * m.sqrt(m.fabs(eff + 1)) - 1)

def morale(comp):
    """
    Calculate morale given composure
    """
    return int(rank(comp)/2)

def get_weight(w):
    """
    Return weight as a string from string or int/float input.
    """
    if type(w) == type(int()) or type(w) == type(float()):
        if int(w) == 1:
            return "Light"
        elif int(w) == 2:
            return "Medium"
        elif int(w) == 3:
            return "Heavy"
        elif w < 1:
            return "Feather"
        else:
            return "Anvil"
    else:
        return str(w)

def get_size(s):
    """
    Return size as a string from string or int/float input.
    """
    if type(s) == type(int()) or type(s) == type(float()):
        if int(s) == 1:
            return "Small"
        elif int(s) == 2:
            return "Medium"
        elif int(s) == 3:
            return "Large"
        elif s < 1:
            return "Tiny"
        else:
            return "Huge"
    else:
        return str(s)

def get_durability(d):
    """
    Return durability as a two-item list from string or list input.
    """
    if type(d) == type(str()):
        if d.lower() == "inf":
            return [m.inf, m.inf]
        else:
            return [0,0]
    else:
        return d

def get_ammo(a):
    """
    Return ammo as an int from string or int input.
    """
    if type(a) == type(str()):
        if a.lower() == "inf":
            return m.inf
        else:
            return 0
    else:
        return a

def roll(expr):
    """
    Roll a single dice expression
    
    INPUTS:
        expr - dice expression string for n dice of each k sides
               can optionally be of the form ndk+/-m
               n,k,m must be integers

    OUTPUTS: (total, total_m, rolls)
        total   - natural total of the roll
        total_m - natural total plus modifier
                  this is included even if the modifier is zero
        rolls   - list of the natural rolls
    """
    dsplit = expr.split('d')
    try:
        n = int(dsplit[0])
    except:
        raise Exception('roll: Invalid dice expression')
        return -1
    if '+' in dsplit[1]:
        modsplit = dsplit[1].split('+')
        try:
            k = int(modsplit[0])
            m = int(modsplit[1])
        except:
            raise Exception('roll: Invalid dice expression')
            return -1
    elif '-' in dsplit[1]:
        modsplit = dsplit[1].split('-')
        try:
            k = int(modsplit[0])
            m = int(modsplit[1])*(-1)
        except:
            raise Exception('roll: Invalid dice expression')
            return -1
    else:
        try:
            k = int(dsplit[1])
            m = 0
        except:
            raise Exception('roll: Invalid dice expression')
            return -1
    total = 0
    rolls = list()
    for i in range(n):
        roll = int(random.random()*k + 1)
        total = total + roll
        rolls.append(roll)
    total_m = total + m
    return (total, total_m, rolls)

def multiroll(exprs):
    """
    Performs a series of rolls
    
    INPUTS:
        exprs - list of dice expressions; see roll()

    OUTPUTS: (rolls_max, rolls_min, totals, totals_m, all_rolls)
        totals    - list of all natural totals
        totals_m  - list of all totals (with modifiers)
        all_rolls - list of all natural roll lists
    """
    totals = list()
    totals_m = list()
    all_rolls = list()
    for expr in exprs:
        (total, total_m, rolls) = roll(expr)
        totals.append(total)
        totals_m.append(total_m)
        all_rolls.append(rolls)
    return (totals, totals_m, all_rolls)
        
def roll_ad(expr, n=2):
    """
    Performs a multiple roll with advantage
    Acts as a thin wrapper for multiroll()

    INPUTS:
        expr - dice expression; see roll()
        n    - (optional)
               number of times to repeat the roll

    OUTPUTS: (rolls_max, rolls_max_m, totals, totals_m, all_rolls)
        rolls_max   - max of all natural totals
        rolls_max_m - max of all totals with modifiers
        totals      - list of all natural totals
        totals_m    - list of all totals (with modifiers)
        all_rolls   - list of all natural roll lists
    """
    exprs = list()
    for i in range(n):
        exprs.append(expr)
    (totals, totals_m, all_rolls) = multiroll(exprs)
    rolls_max = max(totals)
    rolls_max_m = max(totals_m)
    return (rolls_max, rolls_max_m, totals, totals_m, all_rolls)

def roll_disad(expr, n=2):
    """
    Performs a multiple roll with disadvantage
    Acts as a thin wrapper for multiroll()

    INPUTS:
        expr - dice expression; see roll()
        n    - (optional)
               number of times to repeat the roll

    OUTPUTS: (rolls_max, rolls_max_m, totals, totals_m, all_rolls)
        rolls_min   - min of all natural totals
        rolls_min_m - min of all totals with modifiers
        totals      - list of all natural totals
        totals_m    - list of all totals (with modifiers)
        all_rolls   - list of all natural roll lists
    """
    exprs = list()
    for i in range(n):
        exprs.append(expr)
    (totals, totals_m, all_rolls) = multiroll(exprs)
    rolls_max = min(totals)
    rolls_max_m = min(totals_m)
    return (rolls_min, rolls_min_m, totals, totals_m, all_rolls)

def safe_dict(adict, defval=None):
    safe = ddict(lambda: defval)
    if adict != None:
        safe = ddict(lambda: defval)
        for key in adict:
            safe[key] = adict[key]
    return safe

class CharInfo:
    """
    CharInfo class.

    Properties:
        str name
        str race
        str background
        str size
        int speed
        int term
        int credits
        bool inspiration
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_race = "None"
        def_background = "None"
        def_size = "None"
        def_speed = 1
        def_term = 1
        def_credits = 0
        def_inspiration = False
        
        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.race = def_race if safe["race"] == None else safe["race"]
        self.background = def_background if safe["background"] == None else safe["background"]
        self.size = get_size(safe["size"])
        self.speed = def_speed if safe["speed"] == None else safe["speed"]
        self.term = def_term if safe["term"] == None else safe["term"]
        self.credits = def_credits if safe["credits"] == None else safe["credits"]
        self.inspiration = def_inspiration if safe["inspiration"] == None else safe["inspiration"]

    def getall(self):
        return [
                self.name,
                self.race,
                self.background,
                self.term,
                self.size,
                self.speed,
                self.credits,
                self.inspiriation
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {"name": self.name,
                "race": self.race,
                "background": self.background,
                "size": self.size,
                "speed": self.speed,
                "term": self.term,
                "credits": self.credits,
                "inspiration": self.inspiration
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Race: {self.race}\n" + \
               f"Background: {self.background}\n" + \
               f"Size: {self.size}\n" + \
               f"Speed: {self.speed}\n" + \
               f"Term: {self.term}\n" + \
               f"Credits: {self.credits}\n" + \
               f"Inspiration: {self.inspiration}"

class Stats:
    """
    Stats class.

    Properties:
        [int, int] hp
        [int, int] sp
        [int, int] cp
        [int, int] mp
    """
    def __init__(self, yaml_dict=None):
        safe = safe_dict(yaml_dict, [1,1])
        self.hp = safe["hp"]
        self.sp = safe["sp"]
        self.cp = safe["cp"]
        self.mp = safe["mp"]

    def getall(self):
        return [
                self.hp,
                self.sp,
                self.cp,
                self.mp
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {"hp": self.hp,
                "cp": self.cp,
                "sp": self.sp,
                "mp": self.mp
                }

    def __str__(self):
        return f"HP: {self.hp[0]} / {self.hp[1]}\n" + \
               f"CP: {self.cp[0]} / {self.cp[1]}\n" + \
               f"SP: {self.sp[0]} / {self.sp[1]}\n" + \
               f"MP: {self.mp[0]} / {self.mp[1]}"

class Chart:
    """
    Chart class.
    
    Properties:
        Stats obj stats
        [[str]] aa - chart position -> dict key
        defaultdict{str: int} chart_aa - chart values by aa string
        [[int]] chart_nn - chart values position
        [[str]] names (display names)
        int height
        int width

    Methods:
        getmod - calculate roll modifier from any position in the chart
        __getitem__, __setitem__ - get and set raw chart values
    """
    def __init__(self, attr=None, asp=None, skills=None, aa=None, names=None):
        self.chart_aa = ddict(int)
        if attr != None:
            for key in attr:
                self.chart_aa[key] = attr[key]
        if asp != None:
            for key in asp:
                self.chart_aa[key] = asp[key]
        if skills != None:
            for key in skills:
                self.chart_aa[key] = skills[key]

        self.aa = [
                ['physical',  'str',         'dex',       'con'],
                ['offense',   'strike',      'aim',       'grapple'],
                ['mobility',  'force',       'sneak',     'trek'],
                ['defense',   'parry',       'evade',     'block'],
                ['social',    'cha',         'emp',       'tem'],
                ['persuade',  'charm',       'negotiate', 'aggro'],
                ['diplomacy', 'barter',      'insight',   'interrogate'],
                ['deceive',   'mislead',     'disguise',  'suspect'],
                ['mental',    'int',         'wis',       'foc'],
                ['craft',     'appraise',    'medicine',  'engineer'],
                ['awareness', 'investigate', 'survive',   'perceive'],
                ['acuity',    'knowledge',   'resolve',   'concentrate'],
                ['spr',       'spellcast',   'ritual',    'artifact']
                ] if aa == None else aa
        self.names = [
                ['PHYSICAL',  'STR',         'DEX',       'CON'],
                ['Offense',   'Strike',      'Aim',       'Grapple'],
                ['Mobility',  'Force',       'Sneak',     'Trek'],
                ['Defense',   'Parry',       'Evade',     'Block'],
                ['SOCIAL',    'CHA',         'EMP',       'TEM'],
                ['Persuade',  'Charm',       'Negotiate', 'Aggro'],
                ['Diplomacy', 'Barter',      'Insight',   'Interrogate'],
                ['Deceive',   'Mislead',     'Disguise',  'Suspect'],
                ['MENTAL',    'INT',         'WIS',       'FOC'],
                ['Craft',     'Appraise',    'Medicine',  'Engineer'],
                ['Awareness', 'Investigate', 'Survive',   'Perceive'],
                ['Acuity',    'Knowledge',   'Resolve',   'Concentrate'],
                ['SPIRIT',    'Spellcast',   'Ritual',    'Artifact']
                ] if names == None else names
        self.height = len(self.aa)
        self.width = len(self.aa[0])
        
        self.chart_nn = list()
        for i in range(self.height):
            row = list()
            for j in range(self.width):
                row.append(self.chart_aa[self.aa[i][j]])
            self.chart_nn.append(row)
        
    def getpos(self, item):
        i = -1; j = -1
        for m in range(self.height):
            if item in self.aa[m]:
                i = m
                j = self.aa[m].index(item)
                break
        return (i,j)

    def getmod(self, pos):
        if type(pos) == type(str()):
            (i,j) = self.getpos(pos)
        else:
            i,j = pos
        if i < 0 or i >= self.height or j < 0 or j >= self.width:
            return None
        if i == 12 or i % 4 == 0 or j == 0:
            return rank(self.chart_nn[i][j])
        else:
            return self.chart_nn[i][j]\
                    + rank(self.chart_nn[i - i % 4][j])\
                    + rank(self.chart_nn[i][0])
        
    def __getitem__(self, pos):
        if type(pos) == type(str()):
            return self.chart_aa[pos]
        else:
            i,j = pos
            return self.chart_nn[i][j]

    def __setitem__(self, pos, val):
        if type(pos) == type(str()):
            aa = pos
            (i,j) = self.getpos(aa)
        else:
            i,j = pos
            aa = self.aa[i][j]
        self.chart_nn[i][j] = val
        self.chart_aa[aa] = val

    def getall(self):
        return self.chart_nn

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        attributes = dict()
        aspects = dict()
        skills = dict()
        for i in range(self.height):
            for j in range(self.width):
                if i == 12:
                    if j == 0:
                        attributes[self.aa[i][j]] = self.chart_nn[i][j]
                    else:
                        aspects[self.aa[i][j]] = self.chart_nn[i][j]
                elif i % 4 == 0:
                    if j == 0:
                        continue
                    else:
                        attributes[self.aa[i][j]] = self.chart_nn[i][j]
                elif j == 0:
                    aspects[self.aa[i][j]] = self.chart_nn[i][j]
                else:
                    skills[self.aa[i][j]] = self.chart_nn[i][j]
        return (attributes, aspects, skills)

    def __str__(self):
        outstr = ""
        for i in range(self.height):
            topline = ""
            bottomline = ""
            for j in range(self.width):
                topline = topline + f"{self.names[i][j]:<12}"
                bottomline = bottomline + f"{self.chart_nn[i][j]:<12}"
            outstr = outstr + topline + "\n" + bottomline + "\n"
        return outstr[:-1]

class Armor:
    """
    Armor class
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_defense = 0
        def_weight = "None"
        def_durability = [0,0]
        def_effect = "None"
        def_description = "None"

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.defense = def_defense if safe["defense"] == None else safe["defense"]
        self.weight = get_weight(safe["weight"])
        self.durability = get_durability(safe["durability"])
        self.effect = def_effect if safe["effect"] == None else safe["name"]
        self.description = def_description if safe["description"] == None else safe["description"]

    def getall(self):
        return [
                self.name,
                self.defense,
                self.weight,
                self.durability,
                self.effect,
                self.description
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {
                "name": self.name,
                "defense": self.defense,
                "weight": self.weight,
                "durability": self.durability,
                "effect": self.effect,
                "description": self.description
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Defense: {self.defense}\n" + \
               f"Weight: {self.weight}\n" + \
               f"Durability: {self.durability[0] / self.durability[1]}\n" + \
               f"Effect: {self.effect}\n" + \
               f"Description: {self.description}"

class Shield:
    """
    Shield class
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_defense = 0
        def_weight = "None"
        def_durability = [0,0]
        def_effect = "None"
        def_description = "None"

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.defense = def_defense if safe["defense"] == None else safe["defense"]
        self.weight = get_weight(safe["weight"])
        self.durability = get_durability(safe["durability"])
        self.effect = def_effect if safe["effect"] == None else safe["effect"]
        self.description = def_description if safe["description"] == None else safe["description"]

    def getall(self):
        return [
                self.name,
                self.defense,
                self.weight,
                self.durability,
                self.effect,
                self.description
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {
                "name": self.name,
                "defense": self.defense,
                "weight": self.weight,
                "durability": self.durability,
                "effect": self.effect,
                "description": self.description
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Defense: {self.defense}\n" + \
               f"Weight: {self.weight}\n" + \
               f"Durability: {self.durability[0]} / {self.durability[1]}\n" + \
               f"Effect: {self.effect}\n" + \
               f"Description: {self.description}"

class Weapon:
    """
    Weapon class
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_damage = "1d0"
        def_type = "None"
        def_range = 0
        def_ammo = 0
        def_weight = "None"
        def_durability = [0,0]
        def_effect = "None"
        def_description = "None"

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.damage = def_damage if safe["damage"] == None else safe["name"]
        self.type = def_type if safe["type"] == None else safe["type"]
        self.range = def_range if safe["range"] == None else safe["range"]
        self.ammo = get_ammo(safe["ammo"])
        self.weight = get_weight(safe["weight"])
        self.durability = get_durability(safe["durability"])
        self.effect = def_effect if safe["effect"] == None else safe["effect"]
        self.description = def_description if safe["description"] == None else safe["description"]

    def getall(self):
        return [
                self.name,
                self.damage,
                self.type,
                self.range,
                self.ammo,
                self.weight,
                self.durability,
                self.effect
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {
                "name": self.name,
                "damage": self.damage,
                "type": self.type,
                "range": self.range,
                "ammo": self.ammo,
                "weight": self.weight,
                "durability": self.durability,
                "effect": self.effect,
                "description": self.description
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Damage: {self.damage}\n" + \
               f"Type: {self.type}\n" + \
               f"Range: {self.range}\n" + \
               f"Ammo: {self.ammo}\n" + \
               f"Weight: {self.weight}\n" + \
               f"Durability: {self.durability[0]} / {self.durability[1]}\n" + \
               f"Effect: {self.effect}\n" + \
               f"Description: {self.description}"

class Equipment:
    """
    Equipment class
    """
    def __init__(self, yaml_dict=None):
        safe = safe_dict(yaml_dict, "None")
        self.name = safe["name"]
        self.description = safe["description"]

    def getall(self):
        return [
                self.name,
                self.description
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {
                "name": self.name,
                "description": self.description
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Description: {self.description}"

class Consumable:
    """
    Consumable class
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_uses = 1
        def_amount = 1
        def_description = "None"

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.uses = def_uses if safe["uses"] == None else safe["uses"]
        self.amount = def_amount if safe["amount"] == None else safe["amount"]
        self.description = def_description if safe["description"] == None else safe["description"]

    def getall(self):
        return [
                self.name,
                self.uses,
                self.amount,
                self.description
                ]

    def getinfo(self):
        return [
                self.name,
                self.uses,
                self.description
                ]

    def __eq__(self, other):
        return self.getinfo() == other.getinfo() if type(self) == type(other) else False

    def get_yaml(self):
        return {
                "name": self.name,
                "uses": self.uses,
                "amount": self.amount,
                "description": description
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Uses: {self.uses}\n" + \
               f"Amount: {self.amount}\n" + \
               f"Description: {self.description}"

class Crystals:
    """
    Crystals class
    """
    def __init__(self, yaml_dict, colors=None, sizes=None):
        self.chart_aa = ddict(lambda: [0,0,0,0])
        for key in yaml_dict:
            self.chart_aa[key] = yaml_dict[key]

        self.colors = [
                "black",
                "red",
                "orange",
                "yellow",
                "green",
                "blue",
                "violet",
                "white",
                "clear"
                ] if colors == None else colors
        self.sizes = ["s", "m", "l", "x"] if sizes == None else sizes

        self.chart_nn = list()
        for color in self.colors:
            self.chart_nn.append(self.chart_aa[color])

    def __getitem__(self, pos):
        i,j = pos
        if type(i) == type(str()):
            i = self.colors.index(i)
        if type(j) == type(str()):
            j = self.sizes.index(j)
        return self.chart_nn[i][j]

    def __setitem__(self, pos, val):
        i,j = pos
        if type(i) == type(str()):
            ai = i
            i = self.colors.index(ai)
        else:
            ai = self.colors[i]
        if type(j) == type(str()):
            j = self.sizes.index(j)
        self.chart_nn[i][j] = val
        self.chart_aa[ai][j] = val

    def get_yaml(self):
        res = dict()
        for color in self.colors:
            res[color] = self.chart_aa[color]
        return res

    def __str__(self):
        outstr = f"{'':8}"
        for j in range(len(self.sizes)):
            outstr = outstr + f"{self.sizes[j].capitalize():<4}"
        outstr = outstr + "\n"
        for i in range(len(self.colors)):
            outstr = outstr + f"{self.colors[i]:<8}"
            for j in range(len(self.sizes)):
                outstr = outstr + f"{self.chart_nn[i][j]:<4}"
            outstr = outstr + "\n"
        return outstr[:-1]

class Trait:
    """
    Trait class
    """
    def __init__(self, yaml_dict=None):
        safe = safe_dict(yaml_dict, "None")
        self.name = safe["name"]
        self.type = safe["type"]
        self.description = safe["description"]

    def getall(self):
        return [
                self.name,
                self.type,
                self.description
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {
                "name": self.name,
                "type": self.type,
                "description": self.description
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Type: {self.type}\n" + \
               f"Description: {self.description}"

class Ability:
    """
    Ability class
    """
    def __init__(self, yaml_dict=None):
        def_name = "None"
        def_type = "None"
        def_cost = 0
        def_description = "None"

        safe = safe_dict(yaml_dict)
        self.name = def_name if safe["name"] == None else safe["name"]
        self.type = def_type if safe["type"] == None else safe["type"]
        self.cost = def_cost if safe["cost"] == None else safe["cost"]
        self.description = def_description if safe["description"] == None else safe["description"]

    def getall(self):
        return [
                self.name,
                self.abtype,
                self.cost,
                self.description
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {
                "name": self.name,
                "type": self.type,
                "cost": self.cost,
                "description": self.description
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Type: {self.type}\n" + \
               f"Cost: {self.cost} SP\n" + \
               f"Description: {self.description}"

class Course:
    """
    Course class
    """
    def __init__(self, yaml_dict=None):
        safe = safe_dict(yaml_dict, "None")
        self.name = safe["name"]
        self.description = safe["description"]

    def getall(self):
        return [
                self.name,
                self.description
                ]

    def __eq__(self, other):
        return self.getall() == other.getall() if type(self) == type(other) else False

    def get_yaml(self):
        return {
                "name": self.name,
                "description": self.description
                }

    def __str__(self):
        return f"Name: {self.name}\n" + \
               f"Description: {self.description}"

def object_list(yaml_list, constructor):
    """
    Return a list of objects created with the given constructor from a list of
    dictionaries returned by the yaml parser.
    """
    obj_list = list()
    if yaml_list != None:
        for obj in yaml_list:
            obj_list.append(constructor(obj))
    return obj_list

def load_char(charfile="char.yml"):
    """
    Loads a complete character sheet from a given .ini file.
    Assumes the correct format (lol)

    INPUTS:
        charfile - path to the .yml file relative to the executing file

    OUTPUTS: (charinfo, stats, chart, traits, abilities, inventory)
        charinfo  - CharInfo object
        stats     - Stats object
        chart     - Chart object
        traits    - [Trait objects]
        abilities - [Ability objects]
        inventory - [Armor, Shield, [Weapon], [Equipment], [Consumable], Crystal]
    """
    infile = open(charfile, 'r')
    char = safe_dict(yaml.safe_load(infile))
    infile.close()
    
    charinfo = CharInfo(char["general"])

    stats = Stats(char["stats"])

    chart = Chart(char["attributes"], char["aspects"], char["skills"])
    
    armor = Armor(char["armor"])
    shield = Shield(char["shield"])
    weapons = object_list(char["weapons"], Weapon)
    equipment = object_list(char["equipment"], Equipment)
    consumables = object_list(char["consumables"], Consumable)
    crystals = Crystals(char["crystals"])
    inventory = [armor, shield, weapons, equipment, consumables, crystals]

    traits = object_list(char["traits"], Trait)

    abilities = object_list(char["abilities"], Ability)

    return (charinfo, stats, chart, traits, abilities, inventory)
